BUILDFLAGS = -ldflags "-X bitbucket.org/mathildetech/devops-client/pkg/cli/cmd/helper.IsRelease=true"

fmt:
	go fmt ./pkg/...

vet:
	GOPROXY=https://athens.acp.alauda.cn GO111MODULE=on go vet ./...

test:
	GOPROXY=https://athens.acp.alauda.cn GO111MODULE=on go test ./... -v

gen-mock:
	go generate ./pkg/...

gen-jenkins:
	go run main.go gen -t jenkins

gen-nexus:
	go run main.go gen -t nexus

build-cli:
	GOPROXY=https://athens.acp.alauda.cn CGO_ENABLED=0 GO111MODULE=on GOARCH=amd64 GOOS=darwin go build $(BUILDFLAGS) -o bin/acp pkg/cli/main.go
	chmod u+x bin/acp

build-cli-linux:
	GOPROXY=https://athens.acp.alauda.cn CGO_ENABLED=0 GO111MODULE=on GOARCH=amd64 GOOS=linux go build $(BUILDFLAGS) -o bin/acp pkg/cli/main.go
	chmod u+x bin/acp

build-cli-image: build-cli-linux
	docker build . -f Dockerfile -t index.alauda.cn/alaudaorg/devops-cli:dev
