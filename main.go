package main

import (
	"bitbucket.org/mathildetech/devops-client/cmd"
)

func main() {
	cmd.Execute()
}
