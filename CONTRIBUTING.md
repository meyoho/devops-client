## Requirements

* Golang1.11+

## Development

* 采用branch的方式管理版本如v1.0.0/v2.0.0, [参考](https://segmentfault.com/a/1190000019639831?utm_source=tag-newest)。

* master上应该保证一个stable的版本，目前在v1版本的开发中，v1和master默认保持一致性，提pr时同时向v1和master合并。

* 后面的版本如v2或之后版本，先在version分支上进行pr，版本稳定后再将v2合并到master上。

* 对于fix的pr，如果version分支已合并到master，需要向version和master分支同时提pr，否则只向version分支合并。