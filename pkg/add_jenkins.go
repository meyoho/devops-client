package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/jenkins/v1"
)

func init() {
	register(devopsv1.TypeJenkins, versionOpt{
		version:   "v1",
		factory:   v1.NewFactory(),
		isDefault: true,
	})
}
