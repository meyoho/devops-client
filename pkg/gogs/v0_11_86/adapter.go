// Code generated
package v0_11_86

import (
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/go-logr/logr"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/generic"
	"bitbucket.org/mathildetech/devops-client/pkg/gogs/v0_11_86/client"
	"bitbucket.org/mathildetech/devops-client/pkg/gogs/v0_11_86/client/organization"
	"bitbucket.org/mathildetech/devops-client/pkg/gogs/v0_11_86/client/repository"
	"bitbucket.org/mathildetech/devops-client/pkg/gogs/v0_11_86/client/user"
	"bitbucket.org/mathildetech/devops-client/pkg/gogs/v0_11_86/models"
	"bitbucket.org/mathildetech/devops-client/pkg/transport"
	"github.com/dustin/go-humanize"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	v1.NotImplement
	logger     logr.Logger
	client     *client.Gogs
	opts       *v1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ v1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() v1.ClientFactory {
	return func(opts *v1.Options) v1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

func (c *Client) listAllRepos(ctx context.Context) ([]*models.Repository, error) {
	params := user.
		NewUserCurrentListReposParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	res, err := c.client.User.UserCurrentListRepos(params, c.authInfo)
	if err != nil {
		return nil, err
	}
	return res.Payload, nil
}

func (c *Client) listUserOrgs(ctx context.Context) ([]*models.Organization, error) {
	params := organization.
		NewOrgListCurrentUserOrgsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	res, err := c.client.Organization.OrgListCurrentUserOrgs(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return res.Payload, nil
}

func (c *Client) ConvertRemoteRepoToBindingRepo(remoteRepo interface{}, orgs map[string]struct{}) (codeRepo v1.OriginCodeRepository) {
	if remoteRepo == nil {
		return
	}

	if gogsRepo, ok := remoteRepo.(*models.Repository); ok {
		codeRepo = v1.OriginCodeRepository{
			CodeRepoServiceType: v1.CodeRepoServiceTypeGogs,
			ID:                  strconv.FormatInt(gogsRepo.ID, 10),
			Name:                gogsRepo.Name,
			FullName:            gogsRepo.FullName,
			Description:         gogsRepo.Description,
			HTMLURL:             gogsRepo.HTMLURL,
			CloneURL:            gogsRepo.CloneURL,
			SSHURL:              gogsRepo.SSHURL,
			Language:            "",
			Owner: v1.OwnerInRepository{
				ID:   strconv.FormatInt(gogsRepo.Owner.ID, 10),
				Name: gogsRepo.Owner.UserName,
				Type: c.getUserType(orgs, gogsRepo.Owner.UserName),
			},

			CreatedAt: time.Time(gogsRepo.Created),
			PushedAt:  time.Time(gogsRepo.Updated),
			UpdatedAt: time.Time(gogsRepo.Updated),

			Private:      gogsRepo.Private,
			Size:         gogsRepo.Size,
			SizeHumanize: humanize.Bytes(uint64(gogsRepo.Size)),
		}
	}

	return
}

func (c *Client) getUserType(orgs map[string]struct{}, name string) string {
	if _, ok := orgs[name]; ok {
		return v1.OriginCodeRepoRoleTypeOrg
	}
	return v1.OriginCodeRepoOwnerTypeUser
}

// GetRemoteRepos implements CodeRepoService
func (c *Client) GetRemoteRepos(ctx context.Context) (*v1.CodeRepoBindingRepositories, error) {
	repos, err := c.listAllRepos(ctx)
	if err != nil {
		return nil, err
	}

	orgs, err := c.listUserOrgs(ctx)
	if err != nil {
		return nil, err
	}

	dictOrgs := make(map[string]struct{}, len(orgs))
	for _, org := range orgs {
		dictOrgs[org.UserName] = struct{}{}
	}

	dictOwners := make(map[string]*v1.CodeRepositoryOwner)
	for _, repo := range repos {
		ownerKey := repo.Owner.UserName

		if owner, ok := dictOwners[ownerKey]; !ok {
			owner = &v1.CodeRepositoryOwner{
				Type:         c.getUserType(dictOrgs, ownerKey),
				ID:           strconv.FormatInt(repo.Owner.ID, 10),
				Name:         repo.Owner.UserName,
				Email:        repo.Owner.Email.String(),
				HTMLURL:      "",
				AvatarURL:    repo.Owner.AvatarURL,
				DiskUsage:    0,
				Repositories: make([]v1.OriginCodeRepository, 0, 10),
			}
			dictOwners[ownerKey] = owner
		}
		codeRepo := c.ConvertRemoteRepoToBindingRepo(repo, dictOrgs)
		dictOwners[ownerKey].Repositories = append(dictOwners[ownerKey].Repositories, codeRepo)
	}

	result := &v1.CodeRepoBindingRepositories{
		Type:   v1.CodeRepoServiceTypeGogs,
		Owners: []v1.CodeRepositoryOwner{},
	}

	for _, owner := range dictOwners {
		if len(owner.Repositories) > 0 {
			result.Owners = append(result.Owners, *owner)
		}
	}

	return result, nil

}

// GetBranches implements CodeRepoService
func (c *Client) GetBranches(ctx context.Context, owner, repo, repoFullName string) ([]v1.CodeRepoBranch, error) {
	params := repository.
		NewRepoListBranchesParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithOwner(owner).
		WithRepo(repo)
	res, err := c.client.Repository.RepoListBranches(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := make([]v1.CodeRepoBranch, 0, len(res.Payload))
	for _, branch := range res.Payload {
		result = append(result, v1.CodeRepoBranch{
			Name:   branch.Name,
			Commit: branch.Commit.ID,
		})
	}

	return result, nil
}

// CreateCodeRepoProject implements CodeRepoService
func (c *Client) CreateCodeRepoProject(ctx context.Context, opts v1.CreateProjectOptions) (*v1.ProjectData, error) {
	params := organization.
		NewOrgCreateParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithOrganization(&models.CreateOrgOption{
			UserName: &opts.Name,
		})
	res, err := c.client.Organization.OrgCreate(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return gogsOrgAsProjectData(*res.Payload)
}

func gogsOrgAsProjectData(org models.Organization) (*v1.ProjectData, error) {
	data, err := generic.MarshalToMapString(org)
	if err != nil {
		return nil, err
	}

	projectData := &v1.ProjectData{
		Name: org.UserName,
		Annotations: map[string]string{
			"avatarURL":   org.AvatarURL,
			"accessPath":  "/" + org.UserName,
			"description": org.Description,
			"type":        v1.OriginCodeRepoRoleTypeOrg,
		},
		Data: data,
	}

	return projectData, nil
}

func gogsUserAsProjectData(user *models.User) (*v1.ProjectData, error) {
	var projectData = &v1.ProjectData{
		Name: user.UserName,
		Annotations: map[string]string{
			"avatarURL":   user.AvatarURL,
			"webURL":      "/" + user.UserName,
			"description": "",
			"type":        string(v1.OriginCodeRepoOwnerTypeUser),
		},
		Data: map[string]string{
			"login":   user.UserName,
			"id":      strconv.Itoa(int(user.ID)),
			"name":    user.FullName,
			"company": "",
			"type":    "",
			"email":   user.Email.String(),
		},
	}
	return projectData, nil
}

func (c *Client) currentUser(ctx context.Context) (*v1.ProjectData, error) {
	params := user.
		NewUserGetCurrentParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	res, err := c.client.User.UserGetCurrent(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return gogsUserAsProjectData(res.Payload)
}

// ListCodeRepoProjects implements CodeRepoService
func (c *Client) ListCodeRepoProjects(ctx context.Context, opts v1.ListProjectOptions) (*v1.ProjectDataList, error) {
	params := organization.
		NewOrgListCurrentUserOrgsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	res, err := c.client.Organization.OrgListCurrentUserOrgs(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	items := make([]v1.ProjectData, 0, len(res.Payload))
	for _, org := range res.Payload {
		projectData, err := gogsOrgAsProjectData(*org)
		if err != nil {
			return nil, err
		}

		items = append(items, *projectData)
	}

	userProject, err := c.currentUser(ctx)
	if err != nil {
		return nil, err
	}

	items = append(items, *userProject)

	result := &v1.ProjectDataList{
		Items: items,
	}

	return result, nil
}

// GetLatestRepoCommit implements CodeRepoService
func (c *Client) GetLatestRepoCommit(ctx context.Context, repoID, owner, repoName, repoFullName string) (commit *v1.RepositoryCommit, status *v1.HostPortStatus) {
	return nil, &v1.HostPortStatus{
		StatusCode: 404,
		Response:   "Gogs does not support list repo commits",
	}
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(_ context.Context) (*v1.HostPortStatus, error) {
	var (
		url    string
		header map[string]string
	)

	if c.opts.BasicConfig != nil {
		url = c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host + "/api/v1/user"
	}

	if c.opts.APIKey != nil {
		header = map[string]string{
			c.opts.APIKey.Name: c.opts.APIKey.Value,
		}
	}
	return generic.CheckService(url, header)
}
