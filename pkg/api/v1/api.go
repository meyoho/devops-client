package v1

// ClientFactory returns client constructor
type ClientFactory func(*Options) Interface
