// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"strconv"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// RepoCommit repo commit
// swagger:model repoCommit
type RepoCommit struct {

	// author
	Author *RepoCommitAuthor `json:"author,omitempty"`

	// committer
	Committer *RepoCommitCommitter `json:"committer,omitempty"`

	// message
	Message string `json:"message,omitempty"`

	// parents
	Parents []*RepoCommitParentsItems0 `json:"parents"`

	// sha
	Sha string `json:"sha,omitempty"`

	// tree
	Tree *RepoCommitTree `json:"tree,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this repo commit
func (m *RepoCommit) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateAuthor(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateCommitter(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateParents(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateTree(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *RepoCommit) validateAuthor(formats strfmt.Registry) error {

	if swag.IsZero(m.Author) { // not required
		return nil
	}

	if m.Author != nil {
		if err := m.Author.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("author")
			}
			return err
		}
	}

	return nil
}

func (m *RepoCommit) validateCommitter(formats strfmt.Registry) error {

	if swag.IsZero(m.Committer) { // not required
		return nil
	}

	if m.Committer != nil {
		if err := m.Committer.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("committer")
			}
			return err
		}
	}

	return nil
}

func (m *RepoCommit) validateParents(formats strfmt.Registry) error {

	if swag.IsZero(m.Parents) { // not required
		return nil
	}

	for i := 0; i < len(m.Parents); i++ {
		if swag.IsZero(m.Parents[i]) { // not required
			continue
		}

		if m.Parents[i] != nil {
			if err := m.Parents[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("parents" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (m *RepoCommit) validateTree(formats strfmt.Registry) error {

	if swag.IsZero(m.Tree) { // not required
		return nil
	}

	if m.Tree != nil {
		if err := m.Tree.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("tree")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *RepoCommit) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *RepoCommit) UnmarshalBinary(b []byte) error {
	var res RepoCommit
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// RepoCommitAuthor repo commit author
// swagger:model RepoCommitAuthor
type RepoCommitAuthor struct {

	// ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
	Date string `json:"date,omitempty"`

	// email
	Email string `json:"email,omitempty"`

	// name
	Name string `json:"name,omitempty"`
}

// Validate validates this repo commit author
func (m *RepoCommitAuthor) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *RepoCommitAuthor) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *RepoCommitAuthor) UnmarshalBinary(b []byte) error {
	var res RepoCommitAuthor
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// RepoCommitCommitter repo commit committer
// swagger:model RepoCommitCommitter
type RepoCommitCommitter struct {

	// ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
	Date string `json:"date,omitempty"`

	// email
	Email string `json:"email,omitempty"`

	// name
	Name string `json:"name,omitempty"`
}

// Validate validates this repo commit committer
func (m *RepoCommitCommitter) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *RepoCommitCommitter) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *RepoCommitCommitter) UnmarshalBinary(b []byte) error {
	var res RepoCommitCommitter
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// RepoCommitParentsItems0 repo commit parents items0
// swagger:model RepoCommitParentsItems0
type RepoCommitParentsItems0 struct {

	// sha
	Sha string `json:"sha,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this repo commit parents items0
func (m *RepoCommitParentsItems0) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *RepoCommitParentsItems0) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *RepoCommitParentsItems0) UnmarshalBinary(b []byte) error {
	var res RepoCommitParentsItems0
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// RepoCommitTree repo commit tree
// swagger:model RepoCommitTree
type RepoCommitTree struct {

	// sha
	Sha string `json:"sha,omitempty"`

	// url
	URL string `json:"url,omitempty"`
}

// Validate validates this repo commit tree
func (m *RepoCommitTree) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *RepoCommitTree) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *RepoCommitTree) UnmarshalBinary(b []byte) error {
	var res RepoCommitTree
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
