// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetReposOwnerRepoSubscriptionParams creates a new GetReposOwnerRepoSubscriptionParams object
// with the default values initialized.
func NewGetReposOwnerRepoSubscriptionParams() *GetReposOwnerRepoSubscriptionParams {
	var ()
	return &GetReposOwnerRepoSubscriptionParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetReposOwnerRepoSubscriptionParamsWithTimeout creates a new GetReposOwnerRepoSubscriptionParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetReposOwnerRepoSubscriptionParamsWithTimeout(timeout time.Duration) *GetReposOwnerRepoSubscriptionParams {
	var ()
	return &GetReposOwnerRepoSubscriptionParams{

		timeout: timeout,
	}
}

// NewGetReposOwnerRepoSubscriptionParamsWithContext creates a new GetReposOwnerRepoSubscriptionParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetReposOwnerRepoSubscriptionParamsWithContext(ctx context.Context) *GetReposOwnerRepoSubscriptionParams {
	var ()
	return &GetReposOwnerRepoSubscriptionParams{

		Context: ctx,
	}
}

// NewGetReposOwnerRepoSubscriptionParamsWithHTTPClient creates a new GetReposOwnerRepoSubscriptionParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetReposOwnerRepoSubscriptionParamsWithHTTPClient(client *http.Client) *GetReposOwnerRepoSubscriptionParams {
	var ()
	return &GetReposOwnerRepoSubscriptionParams{
		HTTPClient: client,
	}
}

/*GetReposOwnerRepoSubscriptionParams contains all the parameters to send to the API endpoint
for the get repos owner repo subscription operation typically these are written to a http.Request
*/
type GetReposOwnerRepoSubscriptionParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Owner
	  Name of repository owner.

	*/
	Owner string
	/*Repo
	  Name of repository.

	*/
	Repo string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) WithTimeout(timeout time.Duration) *GetReposOwnerRepoSubscriptionParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) WithContext(ctx context.Context) *GetReposOwnerRepoSubscriptionParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) WithHTTPClient(client *http.Client) *GetReposOwnerRepoSubscriptionParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) WithAccept(accept *string) *GetReposOwnerRepoSubscriptionParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithOwner adds the owner to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) WithOwner(owner string) *GetReposOwnerRepoSubscriptionParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithRepo adds the repo to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) WithRepo(repo string) *GetReposOwnerRepoSubscriptionParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the get repos owner repo subscription params
func (o *GetReposOwnerRepoSubscriptionParams) SetRepo(repo string) {
	o.Repo = repo
}

// WriteToRequest writes these params to a swagger request
func (o *GetReposOwnerRepoSubscriptionParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
