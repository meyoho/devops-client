// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetRateLimitParams creates a new GetRateLimitParams object
// with the default values initialized.
func NewGetRateLimitParams() *GetRateLimitParams {
	var ()
	return &GetRateLimitParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetRateLimitParamsWithTimeout creates a new GetRateLimitParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetRateLimitParamsWithTimeout(timeout time.Duration) *GetRateLimitParams {
	var ()
	return &GetRateLimitParams{

		timeout: timeout,
	}
}

// NewGetRateLimitParamsWithContext creates a new GetRateLimitParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetRateLimitParamsWithContext(ctx context.Context) *GetRateLimitParams {
	var ()
	return &GetRateLimitParams{

		Context: ctx,
	}
}

// NewGetRateLimitParamsWithHTTPClient creates a new GetRateLimitParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetRateLimitParamsWithHTTPClient(client *http.Client) *GetRateLimitParams {
	var ()
	return &GetRateLimitParams{
		HTTPClient: client,
	}
}

/*GetRateLimitParams contains all the parameters to send to the API endpoint
for the get rate limit operation typically these are written to a http.Request
*/
type GetRateLimitParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get rate limit params
func (o *GetRateLimitParams) WithTimeout(timeout time.Duration) *GetRateLimitParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get rate limit params
func (o *GetRateLimitParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get rate limit params
func (o *GetRateLimitParams) WithContext(ctx context.Context) *GetRateLimitParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get rate limit params
func (o *GetRateLimitParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get rate limit params
func (o *GetRateLimitParams) WithHTTPClient(client *http.Client) *GetRateLimitParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get rate limit params
func (o *GetRateLimitParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the get rate limit params
func (o *GetRateLimitParams) WithAccept(accept *string) *GetRateLimitParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the get rate limit params
func (o *GetRateLimitParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WriteToRequest writes these params to a swagger request
func (o *GetRateLimitParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
