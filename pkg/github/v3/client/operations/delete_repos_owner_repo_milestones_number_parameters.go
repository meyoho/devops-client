// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewDeleteReposOwnerRepoMilestonesNumberParams creates a new DeleteReposOwnerRepoMilestonesNumberParams object
// with the default values initialized.
func NewDeleteReposOwnerRepoMilestonesNumberParams() *DeleteReposOwnerRepoMilestonesNumberParams {
	var ()
	return &DeleteReposOwnerRepoMilestonesNumberParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteReposOwnerRepoMilestonesNumberParamsWithTimeout creates a new DeleteReposOwnerRepoMilestonesNumberParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewDeleteReposOwnerRepoMilestonesNumberParamsWithTimeout(timeout time.Duration) *DeleteReposOwnerRepoMilestonesNumberParams {
	var ()
	return &DeleteReposOwnerRepoMilestonesNumberParams{

		timeout: timeout,
	}
}

// NewDeleteReposOwnerRepoMilestonesNumberParamsWithContext creates a new DeleteReposOwnerRepoMilestonesNumberParams object
// with the default values initialized, and the ability to set a context for a request
func NewDeleteReposOwnerRepoMilestonesNumberParamsWithContext(ctx context.Context) *DeleteReposOwnerRepoMilestonesNumberParams {
	var ()
	return &DeleteReposOwnerRepoMilestonesNumberParams{

		Context: ctx,
	}
}

// NewDeleteReposOwnerRepoMilestonesNumberParamsWithHTTPClient creates a new DeleteReposOwnerRepoMilestonesNumberParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewDeleteReposOwnerRepoMilestonesNumberParamsWithHTTPClient(client *http.Client) *DeleteReposOwnerRepoMilestonesNumberParams {
	var ()
	return &DeleteReposOwnerRepoMilestonesNumberParams{
		HTTPClient: client,
	}
}

/*DeleteReposOwnerRepoMilestonesNumberParams contains all the parameters to send to the API endpoint
for the delete repos owner repo milestones number operation typically these are written to a http.Request
*/
type DeleteReposOwnerRepoMilestonesNumberParams struct {

	/*Accept
	  Is used to set specified media type.

	*/
	Accept *string
	/*Number
	  Number of milestone.

	*/
	Number int64
	/*Owner
	  Name of repository owner.

	*/
	Owner string
	/*Repo
	  Name of repository.

	*/
	Repo string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) WithTimeout(timeout time.Duration) *DeleteReposOwnerRepoMilestonesNumberParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) WithContext(ctx context.Context) *DeleteReposOwnerRepoMilestonesNumberParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) WithHTTPClient(client *http.Client) *DeleteReposOwnerRepoMilestonesNumberParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccept adds the accept to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) WithAccept(accept *string) *DeleteReposOwnerRepoMilestonesNumberParams {
	o.SetAccept(accept)
	return o
}

// SetAccept adds the accept to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) SetAccept(accept *string) {
	o.Accept = accept
}

// WithNumber adds the number to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) WithNumber(number int64) *DeleteReposOwnerRepoMilestonesNumberParams {
	o.SetNumber(number)
	return o
}

// SetNumber adds the number to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) SetNumber(number int64) {
	o.Number = number
}

// WithOwner adds the owner to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) WithOwner(owner string) *DeleteReposOwnerRepoMilestonesNumberParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithRepo adds the repo to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) WithRepo(repo string) *DeleteReposOwnerRepoMilestonesNumberParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the delete repos owner repo milestones number params
func (o *DeleteReposOwnerRepoMilestonesNumberParams) SetRepo(repo string) {
	o.Repo = repo
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteReposOwnerRepoMilestonesNumberParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.Accept != nil {

		// header param Accept
		if err := r.SetHeaderParam("Accept", *o.Accept); err != nil {
			return err
		}

	}

	// path param number
	if err := r.SetPathParam("number", swag.FormatInt64(o.Number)); err != nil {
		return err
	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
