package test

import (
	"context"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v3 "bitbucket.org/mathildetech/devops-client/pkg/github/v3"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Github tests", func() {
	var (
		// host
		host    = "https://github.test"
		factory v1.ClientFactory
		client  v1.Interface
		opts    *v1.Options

		transport  = gock.NewTransport()
		httpClient = v1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = v3.NewClient()
		opts = v1.NewOptions(v1.NewBasicConfig("github.test", "", []string{}), v1.NewClient(httpClient), v1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetRemoteRepos", func() {
		const (
			repos = `[
				{
					"id": 91062077,
					"node_id": "MDEwOlJlcG9zaXRvcnk5MTA2MjA3Nw==",
					"name": "APICloud-App-Templates",
					"full_name": "test-user/APICloud-App-Templates",
					"private": false,
					"owner": {
						"login": "test-user",
						"id": 16526632,
						"node_id": "MDQ6VXNlcjE2NTI2NjMy",
						"avatar_url": "https://avatars1.githubusercontent.com/u/16526632?v=4",
						"gravatar_id": "",
						"url": "https://api.github.com/users/test-user",
						"html_url": "https://github.com/test-user",
						"followers_url": "https://api.github.com/users/test-user/followers",
						"following_url": "https://api.github.com/users/test-user/following{/other_user}",
						"gists_url": "https://api.github.com/users/test-user/gists{/gist_id}",
						"starred_url": "https://api.github.com/users/test-user/starred{/owner}{/repo}",
						"subscriptions_url": "https://api.github.com/users/test-user/subscriptions",
						"organizations_url": "https://api.github.com/users/test-user/orgs",
						"repos_url": "https://api.github.com/users/test-user/repos",
						"events_url": "https://api.github.com/users/test-user/events{/privacy}",
						"received_events_url": "https://api.github.com/users/test-user/received_events",
						"type": "User",
						"site_admin": false
					},
					"html_url": "https://github.com/test-user/APICloud-App-Templates",
					"description": "APICloud应用案例源码合集，收集一些优秀的基于 APICloud 技术开发的应用模板，欢迎更多的开发者共享优质模板。",
					"fork": true,
					"url": "https://api.github.com/repos/test-user/APICloud-App-Templates",
					"forks_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/forks",
					"keys_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/keys{/key_id}",
					"collaborators_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/collaborators{/collaborator}",
					"teams_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/teams",
					"hooks_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/hooks",
					"issue_events_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/issues/events{/number}",
					"events_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/events",
					"assignees_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/assignees{/user}",
					"branches_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/branches{/branch}",
					"tags_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/tags",
					"blobs_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/git/blobs{/sha}",
					"git_tags_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/git/tags{/sha}",
					"git_refs_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/git/refs{/sha}",
					"trees_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/git/trees{/sha}",
					"statuses_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/statuses/{sha}",
					"languages_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/languages",
					"stargazers_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/stargazers",
					"contributors_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/contributors",
					"subscribers_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/subscribers",
					"subscription_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/subscription",
					"commits_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/commits{/sha}",
					"git_commits_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/git/commits{/sha}",
					"comments_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/comments{/number}",
					"issue_comment_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/issues/comments{/number}",
					"contents_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/contents/{+path}",
					"compare_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/compare/{base}...{head}",
					"merges_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/merges",
					"archive_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/{archive_format}{/ref}",
					"downloads_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/downloads",
					"issues_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/issues{/number}",
					"pulls_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/pulls{/number}",
					"milestones_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/milestones{/number}",
					"notifications_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/notifications{?since,all,participating}",
					"labels_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/labels{/name}",
					"releases_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/releases{/id}",
					"deployments_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/deployments",
					"created_at": "2017-05-12T07:08:13Z",
					"updated_at": "2017-05-12T07:08:15Z",
					"pushed_at": "2017-04-18T12:25:56Z",
					"git_url": "git://github.com/test-user/APICloud-App-Templates.git",
					"ssh_url": "git@github.com:test-user/APICloud-App-Templates.git",
					"clone_url": "https://github.com/test-user/APICloud-App-Templates.git",
					"svn_url": "https://github.com/test-user/APICloud-App-Templates",
					"homepage": "",
					"size": 4035,
					"stargazers_count": 0,
					"watchers_count": 0,
					"language": "HTML",
					"has_issues": false,
					"has_projects": true,
					"has_downloads": true,
					"has_wiki": true,
					"has_pages": false,
					"forks_count": 0,
					"mirror_url": null,
					"archived": false,
					"disabled": false,
					"open_issues_count": 0,
					"license": null,
					"forks": 0,
					"open_issues": 0,
					"watchers": 0,
					"default_branch": "master",
					"permissions": {
						"admin": true,
						"push": true,
						"pull": true
					}
				}
			]`
		)

		BeforeEach(func() {
			gock.New(host).
				Get("/user/repos").
				Reply(200).
				SetHeaders(map[string]string{
					"X-RateLimit-Limit":     "5000",
					"X-RateLimit-Remaining": "4999",
					"X-RateLimit-Reset":     "1561014258",
				}).
				JSON(repos)
		})

		It("list repos", func() {
			repos, err := client.GetRemoteRepos(context.Background())
			Expect(err).To(BeNil())
			Expect(len(repos.Owners)).To(Equal(1))
			Expect(repos.Owners[0].Name).To(Equal("test-user"))
			Expect(len(repos.Owners[0].Repositories)).To(Equal(1))
			Expect(repos.Owners[0].Repositories[0].Name).To(Equal("APICloud-App-Templates"))
		})
	})

	Context("GetBranches", func() {
		const branches = `[
			{
				"name": "master",
				"commit": {
					"sha": "6f01479985cd43258094eb2c113d8d28eefb648b",
					"url": "https://api.github.com/repos/test-user/APICloud-App-Templates/commits/6f01479985cd43258094eb2c113d8d28eefb648b"
				},
				"protected": false,
				"protection": {
					"enabled": false,
					"required_status_checks": {
						"enforcement_level": "off",
						"contexts": []
					}
				},
				"protection_url": "https://api.github.com/repos/test-user/APICloud-App-Templates/branches/master/protection"
			}
		]`
		BeforeEach(func() {
			gock.New(host).
				Get("/repos/test-user/APICloud-App-Templates/branches").
				Reply(200).
				SetHeaders(map[string]string{
					"X-RateLimit-Limit":     "5000",
					"X-RateLimit-Remaining": "4999",
					"X-RateLimit-Reset":     "1561014258",
				}).
				JSON(branches)
		})

		It("get repo branches", func() {
			branches, err := client.GetBranches(context.Background(), "test-user", "APICloud-App-Templates", "")
			Expect(err).To(BeNil())
			Expect(len(branches)).To(Equal(1))
			Expect(branches[0].Name).To(Equal("master"))
			Expect(branches[0].Commit).To(Equal("6f01479985cd43258094eb2c113d8d28eefb648b"))
		})
	})

	Context("CreateCodeRepoProject", func() {
		It("create group", func() {
			_, err := client.CreateCodeRepoProject(context.Background(), v1.CreateProjectOptions{})
			Expect(err).ToNot(BeNil())
			Expect(err.Error()).To(Equal("not supported"))
		})
	})

	Context("ListCodeRepoProjects", func() {
		const (
			orgs = `[
				{
					"login": "cy-test-org-1",
					"id": 51736778,
					"node_id": "MDEyOk9yZ2FuaXphdGlvbjUxNzM2Nzc4",
					"url": "https://api.github.com/orgs/cy-test-org-1",
					"repos_url": "https://api.github.com/orgs/cy-test-org-1/repos",
					"events_url": "https://api.github.com/orgs/cy-test-org-1/events",
					"hooks_url": "https://api.github.com/orgs/cy-test-org-1/hooks",
					"issues_url": "https://api.github.com/orgs/cy-test-org-1/issues",
					"members_url": "https://api.github.com/orgs/cy-test-org-1/members{/member}",
					"public_members_url": "https://api.github.com/orgs/cy-test-org-1/public_members{/member}",
					"avatar_url": "https://avatars0.githubusercontent.com/u/51736778?v=4",
					"description": null
				}
			]`

			user = `{
				"login": "chengjingtao",
				"id": 1991282,
				"node_id": "MDQ6VXNlcjE5OTEyODI=",
				"avatar_url": "https://avatars0.githubusercontent.com/u/1991282?v=4",
				"gravatar_id": "",
				"url": "https://api.github.com/users/chengjingtao",
				"html_url": "https://github.com/chengjingtao",
				"followers_url": "https://api.github.com/users/chengjingtao/followers",
				"following_url": "https://api.github.com/users/chengjingtao/following{/other_user}",
				"gists_url": "https://api.github.com/users/chengjingtao/gists{/gist_id}",
				"starred_url": "https://api.github.com/users/chengjingtao/starred{/owner}{/repo}",
				"subscriptions_url": "https://api.github.com/users/chengjingtao/subscriptions",
				"organizations_url": "https://api.github.com/users/chengjingtao/orgs",
				"repos_url": "https://api.github.com/users/chengjingtao/repos",
				"events_url": "https://api.github.com/users/chengjingtao/events{/privacy}",
				"received_events_url": "https://api.github.com/users/chengjingtao/received_events",
				"type": "User",
				"site_admin": false,
				"name": "chengjt",
				"company": "Alauda",
				"blog": "",
				"location": null,
				"email": "cjt0616@hotmail.com",
				"hireable": null,
				"bio": null,
				"public_repos": 41,
				"public_gists": 1,
				"followers": 11,
				"following": 13,
				"created_at": "2012-07-17T12:39:07Z",
				"updated_at": "2018-12-12T06:49:13Z",
				"private_gists": 0,
				"total_private_repos": 0,
				"owned_private_repos": 0,
				"disk_usage": 16223,
				"collaborators": 0,
				"two_factor_authentication": false,
				"plan": {
					"name": "free",
					"space": 976562499,
					"collaborators": 0,
					"private_repos": 10000
				}
			}`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/user/orgs").
				Reply(200).
				SetHeaders(map[string]string{
					"X-RateLimit-Limit":     "5000",
					"X-RateLimit-Remaining": "4999",
					"X-RateLimit-Reset":     "1561014258",
				}).
				JSON(orgs)
			gock.New(host).
				Get("/user").
				Reply(200).
				SetHeaders(map[string]string{
					"X-RateLimit-Limit":     "5000",
					"X-RateLimit-Remaining": "4999",
					"X-RateLimit-Reset":     "1561014258",
				}).
				JSON(user)
		})

		It("list groups", func() {
			groups, err := client.ListCodeRepoProjects(context.Background(), v1.ListProjectOptions{})
			Expect(err).To(BeNil())
			Expect(groups.Items).To(HaveLen(2))
			Expect(groups.Items[0].Name).To(Equal("cy-test-org-1"))
			Expect(groups.Items[1].Name).To(Equal("chengjingtao"))
		})
	})

})
