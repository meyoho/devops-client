// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// Project project
// swagger:model project
type Project struct {
	Object

	// created on
	// Format: date-time
	CreatedOn strfmt.DateTime `json:"created_on,omitempty"`

	// description
	Description string `json:"description,omitempty"`

	//
	// Indicates whether the project is publicly accessible, or whether it is
	// private to the team and consequently only visible to team members.
	// Note that private projects cannot contain public repositories.
	IsPrivate bool `json:"is_private,omitempty"`

	// The project's key.
	Key string `json:"key,omitempty"`

	// links
	Links *ProjectAO1Links `json:"links,omitempty"`

	// The name of the project.
	Name string `json:"name,omitempty"`

	// owner
	Owner *Team `json:"owner,omitempty"`

	// updated on
	// Format: date-time
	UpdatedOn strfmt.DateTime `json:"updated_on,omitempty"`

	// The project's immutable id.
	UUID string `json:"uuid,omitempty"`

	// a o1 additional properties
	AO1AdditionalProperties map[string]interface{} `json:"-"`
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (m *Project) UnmarshalJSON(raw []byte) error {
	// AO0
	var aO0 Object
	if err := swag.ReadJSON(raw, &aO0); err != nil {
		return err
	}
	m.Object = aO0

	// AO1
	var dataAO1 struct {
		CreatedOn strfmt.DateTime `json:"created_on,omitempty"`

		Description string `json:"description,omitempty"`

		IsPrivate bool `json:"is_private,omitempty"`

		Key string `json:"key,omitempty"`

		Links *ProjectAO1Links `json:"links,omitempty"`

		Name string `json:"name,omitempty"`

		Owner *Team `json:"owner,omitempty"`

		UpdatedOn strfmt.DateTime `json:"updated_on,omitempty"`

		UUID string `json:"uuid,omitempty"`

		AO1AdditionalProperties map[string]interface{} `json:"-"`
	}
	if err := swag.ReadJSON(raw, &dataAO1); err != nil {
		return err
	}

	m.CreatedOn = dataAO1.CreatedOn

	m.Description = dataAO1.Description

	m.IsPrivate = dataAO1.IsPrivate

	m.Key = dataAO1.Key

	m.Links = dataAO1.Links

	m.Name = dataAO1.Name

	m.Owner = dataAO1.Owner

	m.UpdatedOn = dataAO1.UpdatedOn

	m.UUID = dataAO1.UUID

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (m Project) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 2)

	aO0, err := swag.WriteJSON(m.Object)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, aO0)

	var dataAO1 struct {
		CreatedOn strfmt.DateTime `json:"created_on,omitempty"`

		Description string `json:"description,omitempty"`

		IsPrivate bool `json:"is_private,omitempty"`

		Key string `json:"key,omitempty"`

		Links *ProjectAO1Links `json:"links,omitempty"`

		Name string `json:"name,omitempty"`

		Owner *Team `json:"owner,omitempty"`

		UpdatedOn strfmt.DateTime `json:"updated_on,omitempty"`

		UUID string `json:"uuid,omitempty"`

		AO1AdditionalProperties map[string]interface{} `json:"-"`
	}

	dataAO1.CreatedOn = m.CreatedOn

	dataAO1.Description = m.Description

	dataAO1.IsPrivate = m.IsPrivate

	dataAO1.Key = m.Key

	dataAO1.Links = m.Links

	dataAO1.Name = m.Name

	dataAO1.Owner = m.Owner

	dataAO1.UpdatedOn = m.UpdatedOn

	dataAO1.UUID = m.UUID

	jsonDataAO1, errAO1 := swag.WriteJSON(dataAO1)
	if errAO1 != nil {
		return nil, errAO1
	}
	_parts = append(_parts, jsonDataAO1)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this project
func (m *Project) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with Object

	if err := m.validateCreatedOn(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateLinks(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateOwner(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateUpdatedOn(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *Project) validateCreatedOn(formats strfmt.Registry) error {

	if swag.IsZero(m.CreatedOn) { // not required
		return nil
	}

	if err := validate.FormatOf("created_on", "body", "date-time", m.CreatedOn.String(), formats); err != nil {
		return err
	}

	return nil
}

func (m *Project) validateLinks(formats strfmt.Registry) error {

	if swag.IsZero(m.Links) { // not required
		return nil
	}

	if m.Links != nil {
		if err := m.Links.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("links")
			}
			return err
		}
	}

	return nil
}

func (m *Project) validateOwner(formats strfmt.Registry) error {

	if swag.IsZero(m.Owner) { // not required
		return nil
	}

	if m.Owner != nil {
		if err := m.Owner.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("owner")
			}
			return err
		}
	}

	return nil
}

func (m *Project) validateUpdatedOn(formats strfmt.Registry) error {

	if swag.IsZero(m.UpdatedOn) { // not required
		return nil
	}

	if err := validate.FormatOf("updated_on", "body", "date-time", m.UpdatedOn.String(), formats); err != nil {
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (m *Project) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Project) UnmarshalBinary(b []byte) error {
	var res Project
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// ProjectAO1Links project a o1 links
// swagger:model ProjectAO1Links
type ProjectAO1Links struct {

	// avatar
	Avatar *ProjectAO1LinksAvatar `json:"avatar,omitempty"`

	// html
	HTML *ProjectAO1LinksHTML `json:"html,omitempty"`
}

// Validate validates this project a o1 links
func (m *ProjectAO1Links) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateAvatar(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateHTML(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *ProjectAO1Links) validateAvatar(formats strfmt.Registry) error {

	if swag.IsZero(m.Avatar) { // not required
		return nil
	}

	if m.Avatar != nil {
		if err := m.Avatar.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("links" + "." + "avatar")
			}
			return err
		}
	}

	return nil
}

func (m *ProjectAO1Links) validateHTML(formats strfmt.Registry) error {

	if swag.IsZero(m.HTML) { // not required
		return nil
	}

	if m.HTML != nil {
		if err := m.HTML.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("links" + "." + "html")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *ProjectAO1Links) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ProjectAO1Links) UnmarshalBinary(b []byte) error {
	var res ProjectAO1Links
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// ProjectAO1LinksAvatar project a o1 links avatar
// swagger:model ProjectAO1LinksAvatar
type ProjectAO1LinksAvatar struct {

	// href
	// Format: uri
	Href strfmt.URI `json:"href,omitempty"`

	// name
	Name string `json:"name,omitempty"`
}

// Validate validates this project a o1 links avatar
func (m *ProjectAO1LinksAvatar) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateHref(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *ProjectAO1LinksAvatar) validateHref(formats strfmt.Registry) error {

	if swag.IsZero(m.Href) { // not required
		return nil
	}

	if err := validate.FormatOf("links"+"."+"avatar"+"."+"href", "body", "uri", m.Href.String(), formats); err != nil {
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (m *ProjectAO1LinksAvatar) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ProjectAO1LinksAvatar) UnmarshalBinary(b []byte) error {
	var res ProjectAO1LinksAvatar
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// ProjectAO1LinksHTML project a o1 links HTML
// swagger:model ProjectAO1LinksHTML
type ProjectAO1LinksHTML struct {

	// href
	// Format: uri
	Href strfmt.URI `json:"href,omitempty"`

	// name
	Name string `json:"name,omitempty"`
}

// Validate validates this project a o1 links HTML
func (m *ProjectAO1LinksHTML) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateHref(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *ProjectAO1LinksHTML) validateHref(formats strfmt.Registry) error {

	if swag.IsZero(m.Href) { // not required
		return nil
	}

	if err := validate.FormatOf("links"+"."+"html"+"."+"href", "body", "uri", m.Href.String(), formats); err != nil {
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (m *ProjectAO1LinksHTML) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ProjectAO1LinksHTML) UnmarshalBinary(b []byte) error {
	var res ProjectAO1LinksHTML
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
