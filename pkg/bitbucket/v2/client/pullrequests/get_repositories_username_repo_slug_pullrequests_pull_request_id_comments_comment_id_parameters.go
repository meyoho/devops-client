// Code generated by go-swagger; DO NOT EDIT.

package pullrequests

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams creates a new GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams object
// with the default values initialized.
func NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams() *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParamsWithTimeout creates a new GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParamsWithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams{

		timeout: timeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParamsWithContext creates a new GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParamsWithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams{

		Context: ctx,
	}
}

// NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParamsWithHTTPClient creates a new GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParamsWithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams{
		HTTPClient: client,
	}
}

/*GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams contains all the parameters to send to the API endpoint
for the get repositories username repo slug pullrequests pull request ID comments comment ID operation typically these are written to a http.Request
*/
type GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams struct {

	/*CommentID*/
	CommentID string
	/*PullRequestID*/
	PullRequestID string
	/*RepoSlug*/
	RepoSlug string
	/*Username*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) WithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) WithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) WithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithCommentID adds the commentID to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) WithCommentID(commentID string) *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	o.SetCommentID(commentID)
	return o
}

// SetCommentID adds the commentId to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) SetCommentID(commentID string) {
	o.CommentID = commentID
}

// WithPullRequestID adds the pullRequestID to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) WithPullRequestID(pullRequestID string) *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	o.SetPullRequestID(pullRequestID)
	return o
}

// SetPullRequestID adds the pullRequestId to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) SetPullRequestID(pullRequestID string) {
	o.PullRequestID = pullRequestID
}

// WithRepoSlug adds the repoSlug to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) WithRepoSlug(repoSlug string) *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithUsername adds the username to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) WithUsername(username string) *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get repositories username repo slug pullrequests pull request ID comments comment ID params
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetRepositoriesUsernameRepoSlugPullrequestsPullRequestIDCommentsCommentIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param comment_id
	if err := r.SetPathParam("comment_id", o.CommentID); err != nil {
		return err
	}

	// path param pull_request_id
	if err := r.SetPathParam("pull_request_id", o.PullRequestID); err != nil {
		return err
	}

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
