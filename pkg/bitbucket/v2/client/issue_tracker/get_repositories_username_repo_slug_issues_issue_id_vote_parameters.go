// Code generated by go-swagger; DO NOT EDIT.

package issue_tracker

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams creates a new GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams object
// with the default values initialized.
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams() *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParamsWithTimeout creates a new GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParamsWithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams{

		timeout: timeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParamsWithContext creates a new GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParamsWithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams{

		Context: ctx,
	}
}

// NewGetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParamsWithHTTPClient creates a new GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParamsWithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams{
		HTTPClient: client,
	}
}

/*GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams contains all the parameters to send to the API endpoint
for the get repositories username repo slug issues issue ID vote operation typically these are written to a http.Request
*/
type GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams struct {

	/*IssueID
	  The issue id

	*/
	IssueID string
	/*RepoSlug
	  This can either be the repository slug or the UUID of the repository,
	surrounded by curly-braces, for example: `{repository UUID}`.


	*/
	RepoSlug string
	/*Username
	  This can either be the username or the UUID of the account,
	surrounded by curly-braces, for example: `{account UUID}`. An account
	is either a team or user.


	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) WithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) WithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) WithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithIssueID adds the issueID to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) WithIssueID(issueID string) *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams {
	o.SetIssueID(issueID)
	return o
}

// SetIssueID adds the issueId to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) SetIssueID(issueID string) {
	o.IssueID = issueID
}

// WithRepoSlug adds the repoSlug to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) WithRepoSlug(repoSlug string) *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithUsername adds the username to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) WithUsername(username string) *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get repositories username repo slug issues issue ID vote params
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetRepositoriesUsernameRepoSlugIssuesIssueIDVoteParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param issue_id
	if err := r.SetPathParam("issue_id", o.IssueID); err != nil {
		return err
	}

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
