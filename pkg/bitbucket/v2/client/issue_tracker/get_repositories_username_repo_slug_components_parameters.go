// Code generated by go-swagger; DO NOT EDIT.

package issue_tracker

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetRepositoriesUsernameRepoSlugComponentsParams creates a new GetRepositoriesUsernameRepoSlugComponentsParams object
// with the default values initialized.
func NewGetRepositoriesUsernameRepoSlugComponentsParams() *GetRepositoriesUsernameRepoSlugComponentsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugComponentsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugComponentsParamsWithTimeout creates a new GetRepositoriesUsernameRepoSlugComponentsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetRepositoriesUsernameRepoSlugComponentsParamsWithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugComponentsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugComponentsParams{

		timeout: timeout,
	}
}

// NewGetRepositoriesUsernameRepoSlugComponentsParamsWithContext creates a new GetRepositoriesUsernameRepoSlugComponentsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetRepositoriesUsernameRepoSlugComponentsParamsWithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugComponentsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugComponentsParams{

		Context: ctx,
	}
}

// NewGetRepositoriesUsernameRepoSlugComponentsParamsWithHTTPClient creates a new GetRepositoriesUsernameRepoSlugComponentsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetRepositoriesUsernameRepoSlugComponentsParamsWithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugComponentsParams {
	var ()
	return &GetRepositoriesUsernameRepoSlugComponentsParams{
		HTTPClient: client,
	}
}

/*GetRepositoriesUsernameRepoSlugComponentsParams contains all the parameters to send to the API endpoint
for the get repositories username repo slug components operation typically these are written to a http.Request
*/
type GetRepositoriesUsernameRepoSlugComponentsParams struct {

	/*RepoSlug
	  This can either be the repository slug or the UUID of the repository,
	surrounded by curly-braces, for example: `{repository UUID}`.


	*/
	RepoSlug string
	/*Username
	  This can either be the username or the UUID of the account,
	surrounded by curly-braces, for example: `{account UUID}`. An account
	is either a team or user.


	*/
	Username string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get repositories username repo slug components params
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) WithTimeout(timeout time.Duration) *GetRepositoriesUsernameRepoSlugComponentsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get repositories username repo slug components params
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get repositories username repo slug components params
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) WithContext(ctx context.Context) *GetRepositoriesUsernameRepoSlugComponentsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get repositories username repo slug components params
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get repositories username repo slug components params
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) WithHTTPClient(client *http.Client) *GetRepositoriesUsernameRepoSlugComponentsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get repositories username repo slug components params
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithRepoSlug adds the repoSlug to the get repositories username repo slug components params
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) WithRepoSlug(repoSlug string) *GetRepositoriesUsernameRepoSlugComponentsParams {
	o.SetRepoSlug(repoSlug)
	return o
}

// SetRepoSlug adds the repoSlug to the get repositories username repo slug components params
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) SetRepoSlug(repoSlug string) {
	o.RepoSlug = repoSlug
}

// WithUsername adds the username to the get repositories username repo slug components params
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) WithUsername(username string) *GetRepositoriesUsernameRepoSlugComponentsParams {
	o.SetUsername(username)
	return o
}

// SetUsername adds the username to the get repositories username repo slug components params
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) SetUsername(username string) {
	o.Username = username
}

// WriteToRequest writes these params to a swagger request
func (o *GetRepositoriesUsernameRepoSlugComponentsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param repo_slug
	if err := r.SetPathParam("repo_slug", o.RepoSlug); err != nil {
		return err
	}

	// path param username
	if err := r.SetPathParam("username", o.Username); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
