// Code generated by go-swagger; DO NOT EDIT.

package assets

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetAssetsParams creates a new GetAssetsParams object
// with the default values initialized.
func NewGetAssetsParams() *GetAssetsParams {
	var ()
	return &GetAssetsParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetAssetsParamsWithTimeout creates a new GetAssetsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetAssetsParamsWithTimeout(timeout time.Duration) *GetAssetsParams {
	var ()
	return &GetAssetsParams{

		timeout: timeout,
	}
}

// NewGetAssetsParamsWithContext creates a new GetAssetsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetAssetsParamsWithContext(ctx context.Context) *GetAssetsParams {
	var ()
	return &GetAssetsParams{

		Context: ctx,
	}
}

// NewGetAssetsParamsWithHTTPClient creates a new GetAssetsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetAssetsParamsWithHTTPClient(client *http.Client) *GetAssetsParams {
	var ()
	return &GetAssetsParams{
		HTTPClient: client,
	}
}

/*GetAssetsParams contains all the parameters to send to the API endpoint
for the get assets operation typically these are written to a http.Request
*/
type GetAssetsParams struct {

	/*ContinuationToken
	  A token returned by a prior request. If present, the next page of results are returned

	*/
	ContinuationToken *string
	/*Repository
	  Repository from which you would like to retrieve assets.

	*/
	Repository string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get assets params
func (o *GetAssetsParams) WithTimeout(timeout time.Duration) *GetAssetsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get assets params
func (o *GetAssetsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get assets params
func (o *GetAssetsParams) WithContext(ctx context.Context) *GetAssetsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get assets params
func (o *GetAssetsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get assets params
func (o *GetAssetsParams) WithHTTPClient(client *http.Client) *GetAssetsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get assets params
func (o *GetAssetsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithContinuationToken adds the continuationToken to the get assets params
func (o *GetAssetsParams) WithContinuationToken(continuationToken *string) *GetAssetsParams {
	o.SetContinuationToken(continuationToken)
	return o
}

// SetContinuationToken adds the continuationToken to the get assets params
func (o *GetAssetsParams) SetContinuationToken(continuationToken *string) {
	o.ContinuationToken = continuationToken
}

// WithRepository adds the repository to the get assets params
func (o *GetAssetsParams) WithRepository(repository string) *GetAssetsParams {
	o.SetRepository(repository)
	return o
}

// SetRepository adds the repository to the get assets params
func (o *GetAssetsParams) SetRepository(repository string) {
	o.Repository = repository
}

// WriteToRequest writes these params to a swagger request
func (o *GetAssetsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.ContinuationToken != nil {

		// query param continuationToken
		var qrContinuationToken string
		if o.ContinuationToken != nil {
			qrContinuationToken = *o.ContinuationToken
		}
		qContinuationToken := qrContinuationToken
		if qContinuationToken != "" {
			if err := r.SetQueryParam("continuationToken", qContinuationToken); err != nil {
				return err
			}
		}

	}

	// query param repository
	qrRepository := o.Repository
	qRepository := qrRepository
	if qRepository != "" {
		if err := r.SetQueryParam("repository", qRepository); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
