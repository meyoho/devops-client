package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v7 "bitbucket.org/mathildetech/devops-client/pkg/sonarqube/v7"
)

func init() {
	register(devopsv1.TypeSonarQube, versionOpt{
		version:   "v7",
		factory:   v7.NewClient(),
		isDefault: true,
	})
}
