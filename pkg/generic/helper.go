package generic

import (
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"github.com/mitchellh/mapstructure"
)

const (
	ImageDigestSignature = "sha256:"
	ImageDecimal         = 1024
	ImageSizeB           = 1
	ImageSizeKB          = 2
	ImageSizeMB          = 3
	ImageSizeGB          = 4
)

func ConvertSizeToString(intSize int64) (stringSize string) {
	result, times := float32(intSize), 1
	for result > ImageDecimal {
		result = result / ImageDecimal
		times += 1
	}
	switch times {
	case ImageSizeB:
		return fmt.Sprintf("%dB", intSize)
	case ImageSizeKB:
		return fmt.Sprintf("%.2fKB", result)
	case ImageSizeMB:
		return fmt.Sprintf("%.2fMB", result)
	case ImageSizeGB:
		return fmt.Sprintf("%.2fGB", result)
	default:
		return fmt.Sprintf("%d out of size", intSize)
	}
}

func MarshalToMapString(data interface{}) (map[string]string, error) {
	mapData := &map[string]interface{}{}
	err := mapstructure.Decode(data, mapData)
	if err != nil {
		return nil, err
	}

	result := map[string]string{}
	for key, value := range *mapData {
		str, err := marshalToString(value)
		if err != nil {
			return nil, err
		}
		result[key] = str
	}

	return result, nil
}

func marshalToString(simpleObj interface{}) (string, error) {
	if simpleObj == nil {
		return "", nil
	}

	var originStringKind = map[reflect.Kind]string{
		reflect.Bool: "", reflect.Int: "", reflect.Int8: "", reflect.Int16: "", reflect.Int32: "", reflect.Int64: "",
		reflect.Uint: "", reflect.Uint8: "", reflect.Uint16: "", reflect.Uint32: "", reflect.Uint64: "", reflect.Uintptr: "",
		reflect.Float32: "", reflect.Float64: "", reflect.String: "",
	}

	t := reflect.TypeOf(simpleObj)
	if _, ok := originStringKind[t.Kind()]; ok {
		return fmt.Sprint(simpleObj), nil
	}

	bts, err := json.Marshal(simpleObj)
	if err != nil {
		return "", err
	}
	return string(bts), nil
}

func ConvertStringToTime(t string) time.Time {
	parsedTime, _ := time.Parse(time.RFC3339Nano, t)
	return parsedTime
}
