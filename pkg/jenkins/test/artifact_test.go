package test

import (
	"bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	jenkinsv1 "bitbucket.org/mathildetech/devops-client/pkg/jenkins/v1"
	"bytes"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
	"io"
	"io/ioutil"
)

var _ = Describe("get artifact info from Jenkins", func() {
	var (
		host       = "https://localhost"
		factory    v1.ClientFactory
		client     v1.Interface
		opts       *v1.Options
		reader     bytes.Buffer
		httpClient = v1.NewDefaultClient()
	)

	BeforeEach(func() {
		factory = jenkinsv1.NewFactory()
		opts = v1.NewOptions(v1.NewBasicAuth("admin", "admin"), v1.NewBasicConfig("localhost", "", []string{"https"}), v1.NewBearerToken("token"), v1.NewClient(httpClient))
		client = factory(opts)
		gock.InterceptClient(httpClient)
	})

	AfterEach(func() {
		gock.Off()
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("Submit pipeline input", func() {
		const artifact = `
		[
			{
				"_class": "io.jenkins.blueocean.service.embedded.rest.ArtifactImpl",
				"_links": {
					"self": {
						"_class": "io.jenkins.blueocean.rest.hal.Link",
						"href": "/blue/rest/organizations/jenkins/pipelines/zpyu/pipelines/zpyutest/runs/6/artifacts/io.jenkins.blueocean.service.embedded.rest.ArtifactImpl%253A1.dat/"
					}
				},
				"downloadable": true,
				"id": "io.jenkins.blueocean.service.embedded.rest.ArtifactImpl:1.dat",
				"name": "1.dat",
				"path": "1.dat",
				"size": 1024,
				"url": "/job/zpyu/job/zpyutest/6/artifact/1.dat"
			},
			{
				"_class": "io.jenkins.blueocean.service.embedded.rest.ArtifactImpl",
				"_links": {
					"self": {
						"_class": "io.jenkins.blueocean.rest.hal.Link",
						"href": "/blue/rest/organizations/jenkins/pipelines/zpyu/pipelines/zpyutest/runs/6/artifacts/io.jenkins.blueocean.service.embedded.rest.ArtifactImpl%253A10.dat/"
					}
				},
				"downloadable": true,
				"id": "io.jenkins.blueocean.service.embedded.rest.ArtifactImpl:10.dat",
				"name": "10.dat",
				"path": "10.dat",
				"size": 1024,
				"url": "/job/zpyu/job/zpyutest/6/artifact/10.dat"
			}
		]
		`
		Context("Normal pipeline", func() {
			BeforeEach(func() {
				bytes, err := ioutil.ReadFile("test.json")
				if err != nil {
					panic(err)
				}
				gock.New(host).
					Get("/blue/rest/organizations/organization/pipelines/namespace/pipelines/namespace-pipelinename/runs/run/artifacts").
					MatchParam("start", "0").
					MatchParam("limit", "500").
					Reply(200).
					JSON(string(bytes))
				gock.New(host).
					Get("/blue/rest/organizations/organization/pipelines/namespace/pipelines/namespace-pipelinename/runs/run/artifacts").
					MatchParam("start", "500").
					MatchParam("limit", "500").
					Reply(200).
					JSON(artifact)

			})

			It("Get test report summary from a normal pipeline job", func() {
				artifactlist, err := client.GetArtifactList(context.Background(), "namespace", "pipelinename", "organization", "run")
				Expect(len(artifactlist)).To(Equal(502))
				Expect(artifactlist[500].URL).To(Equal("/job/zpyu/job/zpyutest/6/artifact/1.dat"))
				Expect(err).To(BeNil())
			})

		})
		Context("Get download log file", func() {

			It("Get download file", func() {

				reader = *bytes.NewBuffer([]byte(`data`))
				gock.New(host).
					Get("job/namespace/job/namespace-name/run/artifact/zpyu").
					Reply(200).Body(&reader).AddHeader("Content-Type", "application/octet-stream")

				resp, contentype, err := client.DownloadArtifact(context.Background(), "namespace", "name", "run", "zpyu")
				Expect(err).To(BeNil())
				result := bytes.Buffer{}
				io.Copy(&result, resp)
				Expect(result.Bytes()).To(Equal([]byte("data")))
				Expect(contentype).To(Equal("application/octet-stream"))

			})
		})
	})
})
