// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetProjectBranchesListParams creates a new GetProjectBranchesListParams object
// with the default values initialized.
func NewGetProjectBranchesListParams() *GetProjectBranchesListParams {
	var ()
	return &GetProjectBranchesListParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetProjectBranchesListParamsWithTimeout creates a new GetProjectBranchesListParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetProjectBranchesListParamsWithTimeout(timeout time.Duration) *GetProjectBranchesListParams {
	var ()
	return &GetProjectBranchesListParams{

		timeout: timeout,
	}
}

// NewGetProjectBranchesListParamsWithContext creates a new GetProjectBranchesListParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetProjectBranchesListParamsWithContext(ctx context.Context) *GetProjectBranchesListParams {
	var ()
	return &GetProjectBranchesListParams{

		Context: ctx,
	}
}

// NewGetProjectBranchesListParamsWithHTTPClient creates a new GetProjectBranchesListParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetProjectBranchesListParamsWithHTTPClient(client *http.Client) *GetProjectBranchesListParams {
	var ()
	return &GetProjectBranchesListParams{
		HTTPClient: client,
	}
}

/*GetProjectBranchesListParams contains all the parameters to send to the API endpoint
for the get project branches list operation typically these are written to a http.Request
*/
type GetProjectBranchesListParams struct {

	/*Project*/
	Project string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get project branches list params
func (o *GetProjectBranchesListParams) WithTimeout(timeout time.Duration) *GetProjectBranchesListParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get project branches list params
func (o *GetProjectBranchesListParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get project branches list params
func (o *GetProjectBranchesListParams) WithContext(ctx context.Context) *GetProjectBranchesListParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get project branches list params
func (o *GetProjectBranchesListParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get project branches list params
func (o *GetProjectBranchesListParams) WithHTTPClient(client *http.Client) *GetProjectBranchesListParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get project branches list params
func (o *GetProjectBranchesListParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithProject adds the project to the get project branches list params
func (o *GetProjectBranchesListParams) WithProject(project string) *GetProjectBranchesListParams {
	o.SetProject(project)
	return o
}

// SetProject adds the project to the get project branches list params
func (o *GetProjectBranchesListParams) SetProject(project string) {
	o.Project = project
}

// WriteToRequest writes these params to a swagger request
func (o *GetProjectBranchesListParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// query param project
	qrProject := o.Project
	qProject := qrProject
	if qProject != "" {
		if err := r.SetQueryParam("project", qProject); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
