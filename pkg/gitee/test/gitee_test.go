package test

import (
	"context"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v5 "bitbucket.org/mathildetech/devops-client/pkg/gitee/v5"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Gitee tests", func() {
	var (
		// host
		host    = "https://gitee.test"
		factory v1.ClientFactory
		client  v1.Interface
		opts    *v1.Options

		transport  = gock.NewTransport()
		httpClient = v1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = v5.NewClient()
		opts = v1.NewOptions(v1.NewBasicConfig("gitee.test", "", []string{}), v1.NewClient(httpClient), v1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		gock.OffAll()
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetRemoteRepos", func() {
		const (
			repos = `[
				{
					"id": 6070152,
					"full_name": "pwn2ownggg/test-repo-1",
					"human_name": "pwn2own/test-repo-1",
					"url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1",
					"namespace": {
						"id": 4995118,
						"type": "personal",
						"name": "pwn2own",
						"path": "pwn2ownggg",
						"html_url": "https://gitee.com/pwn2ownggg"
					},
					"path": "test-repo-1",
					"name": "test-repo-1",
					"owner": {
						"id": 5059597,
						"login": "pwn2ownggg",
						"name": "pwn2own",
						"avatar_url": "https://gitee.com/assets/no_portrait.png",
						"url": "https://gitee.com/api/v5/users/pwn2ownggg",
						"html_url": "https://gitee.com/pwn2ownggg",
						"followers_url": "https://gitee.com/api/v5/users/pwn2ownggg/followers",
						"following_url": "https://gitee.com/api/v5/users/pwn2ownggg/following_url{/other_user}",
						"gists_url": "https://gitee.com/api/v5/users/pwn2ownggg/gists{/gist_id}",
						"starred_url": "https://gitee.com/api/v5/users/pwn2ownggg/starred{/owner}{/repo}",
						"subscriptions_url": "https://gitee.com/api/v5/users/pwn2ownggg/subscriptions",
						"organizations_url": "https://gitee.com/api/v5/users/pwn2ownggg/orgs",
						"repos_url": "https://gitee.com/api/v5/users/pwn2ownggg/repos",
						"events_url": "https://gitee.com/api/v5/users/pwn2ownggg/events{/privacy}",
						"received_events_url": "https://gitee.com/api/v5/users/pwn2ownggg/received_events",
						"type": "User",
						"site_admin": false
					},
					"description": "",
					"private": true,
					"public": false,
					"internal": false,
					"fork": false,
					"html_url": "https://gitee.com/pwn2ownggg/test-repo-1.git",
					"ssh_url": "git@gitee.com:pwn2ownggg/test-repo-1.git",
					"forks_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/forks",
					"keys_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/keys{/key_id}",
					"collaborators_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/collaborators{/collaborator}",
					"hooks_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/hooks",
					"branches_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/branches{/branch}",
					"tags_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/tags",
					"blobs_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/blobs{/sha}",
					"stargazers_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/stargazers",
					"contributors_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/contributors",
					"commits_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/commits{/sha}",
					"comments_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/comments{/number}",
					"issue_comment_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/issues/comments{/number}",
					"issues_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/issues{/number}",
					"pulls_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/pulls{/number}",
					"milestones_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/milestones{/number}",
					"notifications_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/notifications{?since,all,participating}",
					"labels_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/labels{/name}",
					"releases_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/releases{/id}",
					"recommend": false,
					"homepage": null,
					"language": "Java",
					"forks_count": 0,
					"stargazers_count": 0,
					"watchers_count": 1,
					"default_branch": "master",
					"open_issues_count": 0,
					"has_issues": true,
					"has_wiki": true,
					"pull_requests_enabled": true,
					"has_page": false,
					"license": null,
					"outsourced": false,
					"project_creator": "pwn2ownggg",
					"members": [
						"pwn2ownggg"
					],
					"pushed_at": "2019-06-11T17:46:58+08:00",
					"created_at": "2019-06-05T16:43:36+08:00",
					"updated_at": "2019-06-11T17:46:58+08:00",
					"parent": null,
					"paas": null,
					"stared": false,
					"watched": true,
					"permission": {
						"pull": true,
						"push": true,
						"admin": true
					},
					"relation": "master"
				}
			]`

			orgs = `[
				{
					"id": 5007309,
					"login": "ttt-ccc",
					"url": "https://gitee.com/api/v5/orgs/ttt-ccc",
					"avatar_url": "",
					"repos_url": "https://gitee.com/api/v5/orgs/ttt-ccc/repos",
					"events_url": "https://gitee.com/api/v5/orgs/ttt-ccc/events",
					"members_url": "https://gitee.com/api/v5/orgs/ttt-ccc/members{/member}",
					"description": null
				},
				{
					"id": 5007313,
					"login": "ttt-cccc",
					"url": "https://gitee.com/api/v5/orgs/ttt-cccc",
					"avatar_url": "",
					"repos_url": "https://gitee.com/api/v5/orgs/ttt-cccc/repos",
					"events_url": "https://gitee.com/api/v5/orgs/ttt-cccc/events",
					"members_url": "https://gitee.com/api/v5/orgs/ttt-cccc/members{/member}",
					"description": null
				}
			]`
		)

		BeforeEach(func() {
			gock.New(host).
				Get("/user/repos").
				Reply(200).
				JSON(repos)

			gock.New(host).
				Get("/user/orgs").
				Reply(200).
				JSON(orgs)
		})

		It("list repos", func() {
			repos, err := client.GetRemoteRepos(context.Background())
			Expect(err).To(BeNil())
			Expect(len(repos.Owners)).To(Equal(1))
			Expect(repos.Owners[0].Name).To(Equal("pwn2ownggg"))
			Expect(len(repos.Owners[0].Repositories)).To(Equal(1))
			Expect(repos.Owners[0].Repositories[0].Name).To(Equal("test-repo-1"))
		})
	})

	Context("GetBranches", func() {
		const branches = `[
			{
				"name": "fix",
				"commit": {
					"sha": "237efa4f84eada297b757b60f07b0fdc79bd903b",
					"url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/commits/237efa4f84eada297b757b60f07b0fdc79bd903b"
				},
				"protected": false,
				"protection_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/branches/fix/protection"
			},
			{
				"name": "master",
				"commit": {
					"sha": "237efa4f84eada297b757b60f07b0fdc79bd903b",
					"url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/commits/237efa4f84eada297b757b60f07b0fdc79bd903b"
				},
				"protected": false,
				"protection_url": "https://gitee.com/api/v5/repos/pwn2ownggg/test-repo-1/branches/master/protection"
			}
		]`
		BeforeEach(func() {
			gock.New(host).
				Get("/repos/pwn2ownggg/test-repo-1/branches").
				Reply(200).
				JSON(branches)
		})

		It("get repo branches", func() {
			branches, err := client.GetBranches(context.Background(), "admin_pwn2ownggg", "test-repo-1", "pwn2ownggg/test-repo-1")
			Expect(err).To(BeNil())
			Expect(len(branches)).To(Equal(2))
			Expect(branches[0].Name).To(Equal("fix"))
			Expect(branches[0].Commit).To(Equal("237efa4f84eada297b757b60f07b0fdc79bd903b"))
		})
	})

	Context("CreateCodeRepoProject", func() {
		const (
			org = `{
				"id": 5029193,
				"login": "cy-ttt-org",
				"url": "https://gitee.com/api/v5/orgs/cy-ttt-org",
				"avatar_url": "",
				"repos_url": "https://gitee.com/api/v5/orgs/cy-ttt-org/repos",
				"events_url": "https://gitee.com/api/v5/orgs/cy-ttt-org/events",
				"members_url": "https://gitee.com/api/v5/orgs/cy-ttt-org/members{/member}",
				"description": null
			}`
		)

		BeforeEach(func() {
			gock.New(host).
				Post("/users/organization").
				Reply(201).
				JSON(org)
		})

		It("create group", func() {
			org, err := client.CreateCodeRepoProject(context.Background(), v1.CreateProjectOptions{Name: "cy-ttt-org"})
			Expect(err).To(BeNil())
			Expect(org.Name).To(Equal("cy-ttt-org"))
		})
	})

	Context("ListCodeRepoProjects", func() {
		const (
			orgs = `[
				{
					"id": 5007309,
					"login": "ttt-ccc",
					"url": "https://gitee.com/api/v5/orgs/ttt-ccc",
					"avatar_url": "",
					"repos_url": "https://gitee.com/api/v5/orgs/ttt-ccc/repos",
					"events_url": "https://gitee.com/api/v5/orgs/ttt-ccc/events",
					"members_url": "https://gitee.com/api/v5/orgs/ttt-ccc/members{/member}",
					"description": null
				},
				{
					"id": 5007313,
					"login": "ttt-cccc",
					"url": "https://gitee.com/api/v5/orgs/ttt-cccc",
					"avatar_url": "",
					"repos_url": "https://gitee.com/api/v5/orgs/ttt-cccc/repos",
					"events_url": "https://gitee.com/api/v5/orgs/ttt-cccc/events",
					"members_url": "https://gitee.com/api/v5/orgs/ttt-cccc/members{/member}",
					"description": null
				}
			]`

			user = `{
				"id": 303622,
				"login": "chengjt",
				"name": "程静涛",
				"avatar_url": "https://gitee.com/assets/no_portrait.png",
				"url": "https://gitee.com/api/v5/users/chengjt",
				"html_url": "https://gitee.com/chengjt",
				"followers_url": "https://gitee.com/api/v5/users/chengjt/followers",
				"following_url": "https://gitee.com/api/v5/users/chengjt/following_url{/other_user}",
				"gists_url": "https://gitee.com/api/v5/users/chengjt/gists{/gist_id}",
				"starred_url": "https://gitee.com/api/v5/users/chengjt/starred{/owner}{/repo}",
				"subscriptions_url": "https://gitee.com/api/v5/users/chengjt/subscriptions",
				"organizations_url": "https://gitee.com/api/v5/users/chengjt/orgs",
				"repos_url": "https://gitee.com/api/v5/users/chengjt/repos",
				"events_url": "https://gitee.com/api/v5/users/chengjt/events{/privacy}",
				"received_events_url": "https://gitee.com/api/v5/users/chengjt/received_events",
				"type": "User",
				"site_admin": false,
				"blog": null,
				"weibo": null,
				"bio": null,
				"public_repos": 3,
				"public_gists": 1,
				"followers": 0,
				"following": 0,
				"stared": 2,
				"watched": 9,
				"created_at": "2015-01-15T13:02:20+08:00",
				"updated_at": "2019-03-22T12:42:10+08:00",
				"email": "1016890794@qq.com",
				"phone": "15101680849",
				"private_token": "RZosqtKMSN23VKEMGMi9",
				"total_repos": 1000,
				"owned_repos": 9,
				"total_private_repos": 1000,
				"owned_private_repos": 6,
				"private_gists": 0,
				"address": {
					"name": null,
					"tel": null,
					"address": null,
					"province": null,
					"city": null,
					"zip_code": null,
					"comment": null
				}
			}`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/user/orgs").
				Reply(200).
				JSON(orgs)

			gock.New(host).
				Get("/user").
				Reply(200).
				JSON(user)
		})

		It("list groups", func() {
			groups, err := client.ListCodeRepoProjects(context.Background(), v1.ListProjectOptions{})
			Expect(err).To(BeNil())
			Expect(groups.Items).To(HaveLen(3))
			Expect(groups.Items[0].Name).To(Equal("ttt-ccc"))
		})
	})

})
