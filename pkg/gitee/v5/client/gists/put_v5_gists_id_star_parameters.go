// Code generated by go-swagger; DO NOT EDIT.

package gists

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPutV5GistsIDStarParams creates a new PutV5GistsIDStarParams object
// with the default values initialized.
func NewPutV5GistsIDStarParams() *PutV5GistsIDStarParams {
	var ()
	return &PutV5GistsIDStarParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutV5GistsIDStarParamsWithTimeout creates a new PutV5GistsIDStarParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutV5GistsIDStarParamsWithTimeout(timeout time.Duration) *PutV5GistsIDStarParams {
	var ()
	return &PutV5GistsIDStarParams{

		timeout: timeout,
	}
}

// NewPutV5GistsIDStarParamsWithContext creates a new PutV5GistsIDStarParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutV5GistsIDStarParamsWithContext(ctx context.Context) *PutV5GistsIDStarParams {
	var ()
	return &PutV5GistsIDStarParams{

		Context: ctx,
	}
}

// NewPutV5GistsIDStarParamsWithHTTPClient creates a new PutV5GistsIDStarParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutV5GistsIDStarParamsWithHTTPClient(client *http.Client) *PutV5GistsIDStarParams {
	var ()
	return &PutV5GistsIDStarParams{
		HTTPClient: client,
	}
}

/*PutV5GistsIDStarParams contains all the parameters to send to the API endpoint
for the put v5 gists Id star operation typically these are written to a http.Request
*/
type PutV5GistsIDStarParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*ID
	  代码片段的ID

	*/
	ID string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put v5 gists Id star params
func (o *PutV5GistsIDStarParams) WithTimeout(timeout time.Duration) *PutV5GistsIDStarParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put v5 gists Id star params
func (o *PutV5GistsIDStarParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put v5 gists Id star params
func (o *PutV5GistsIDStarParams) WithContext(ctx context.Context) *PutV5GistsIDStarParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put v5 gists Id star params
func (o *PutV5GistsIDStarParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put v5 gists Id star params
func (o *PutV5GistsIDStarParams) WithHTTPClient(client *http.Client) *PutV5GistsIDStarParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put v5 gists Id star params
func (o *PutV5GistsIDStarParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the put v5 gists Id star params
func (o *PutV5GistsIDStarParams) WithAccessToken(accessToken *string) *PutV5GistsIDStarParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the put v5 gists Id star params
func (o *PutV5GistsIDStarParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithID adds the id to the put v5 gists Id star params
func (o *PutV5GistsIDStarParams) WithID(id string) *PutV5GistsIDStarParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the put v5 gists Id star params
func (o *PutV5GistsIDStarParams) SetID(id string) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *PutV5GistsIDStarParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// form param access_token
		var frAccessToken string
		if o.AccessToken != nil {
			frAccessToken = *o.AccessToken
		}
		fAccessToken := frAccessToken
		if fAccessToken != "" {
			if err := r.SetFormParam("access_token", fAccessToken); err != nil {
				return err
			}
		}

	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
