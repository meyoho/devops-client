// Code generated by go-swagger; DO NOT EDIT.

package gists

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPatchV5GistsGistIDCommentsIDParams creates a new PatchV5GistsGistIDCommentsIDParams object
// with the default values initialized.
func NewPatchV5GistsGistIDCommentsIDParams() *PatchV5GistsGistIDCommentsIDParams {
	var ()
	return &PatchV5GistsGistIDCommentsIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPatchV5GistsGistIDCommentsIDParamsWithTimeout creates a new PatchV5GistsGistIDCommentsIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPatchV5GistsGistIDCommentsIDParamsWithTimeout(timeout time.Duration) *PatchV5GistsGistIDCommentsIDParams {
	var ()
	return &PatchV5GistsGistIDCommentsIDParams{

		timeout: timeout,
	}
}

// NewPatchV5GistsGistIDCommentsIDParamsWithContext creates a new PatchV5GistsGistIDCommentsIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewPatchV5GistsGistIDCommentsIDParamsWithContext(ctx context.Context) *PatchV5GistsGistIDCommentsIDParams {
	var ()
	return &PatchV5GistsGistIDCommentsIDParams{

		Context: ctx,
	}
}

// NewPatchV5GistsGistIDCommentsIDParamsWithHTTPClient creates a new PatchV5GistsGistIDCommentsIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPatchV5GistsGistIDCommentsIDParamsWithHTTPClient(client *http.Client) *PatchV5GistsGistIDCommentsIDParams {
	var ()
	return &PatchV5GistsGistIDCommentsIDParams{
		HTTPClient: client,
	}
}

/*PatchV5GistsGistIDCommentsIDParams contains all the parameters to send to the API endpoint
for the patch v5 gists gist Id comments Id operation typically these are written to a http.Request
*/
type PatchV5GistsGistIDCommentsIDParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*Body
	  评论内容

	*/
	Body string
	/*GistID
	  代码片段的ID

	*/
	GistID string
	/*ID
	  评论的ID

	*/
	ID int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) WithTimeout(timeout time.Duration) *PatchV5GistsGistIDCommentsIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) WithContext(ctx context.Context) *PatchV5GistsGistIDCommentsIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) WithHTTPClient(client *http.Client) *PatchV5GistsGistIDCommentsIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) WithAccessToken(accessToken *string) *PatchV5GistsGistIDCommentsIDParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithBody adds the body to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) WithBody(body string) *PatchV5GistsGistIDCommentsIDParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) SetBody(body string) {
	o.Body = body
}

// WithGistID adds the gistID to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) WithGistID(gistID string) *PatchV5GistsGistIDCommentsIDParams {
	o.SetGistID(gistID)
	return o
}

// SetGistID adds the gistId to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) SetGistID(gistID string) {
	o.GistID = gistID
}

// WithID adds the id to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) WithID(id int32) *PatchV5GistsGistIDCommentsIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the patch v5 gists gist Id comments Id params
func (o *PatchV5GistsGistIDCommentsIDParams) SetID(id int32) {
	o.ID = id
}

// WriteToRequest writes these params to a swagger request
func (o *PatchV5GistsGistIDCommentsIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// form param access_token
		var frAccessToken string
		if o.AccessToken != nil {
			frAccessToken = *o.AccessToken
		}
		fAccessToken := frAccessToken
		if fAccessToken != "" {
			if err := r.SetFormParam("access_token", fAccessToken); err != nil {
				return err
			}
		}

	}

	// form param body
	frBody := o.Body
	fBody := frBody
	if fBody != "" {
		if err := r.SetFormParam("body", fBody); err != nil {
			return err
		}
	}

	// path param gist_id
	if err := r.SetPathParam("gist_id", o.GistID); err != nil {
		return err
	}

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt32(o.ID)); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
