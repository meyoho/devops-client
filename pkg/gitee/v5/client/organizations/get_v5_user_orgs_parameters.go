// Code generated by go-swagger; DO NOT EDIT.

package organizations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV5UserOrgsParams creates a new GetV5UserOrgsParams object
// with the default values initialized.
func NewGetV5UserOrgsParams() *GetV5UserOrgsParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
	)
	return &GetV5UserOrgsParams{
		Page:    &pageDefault,
		PerPage: &perPageDefault,

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV5UserOrgsParamsWithTimeout creates a new GetV5UserOrgsParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV5UserOrgsParamsWithTimeout(timeout time.Duration) *GetV5UserOrgsParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
	)
	return &GetV5UserOrgsParams{
		Page:    &pageDefault,
		PerPage: &perPageDefault,

		timeout: timeout,
	}
}

// NewGetV5UserOrgsParamsWithContext creates a new GetV5UserOrgsParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV5UserOrgsParamsWithContext(ctx context.Context) *GetV5UserOrgsParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
	)
	return &GetV5UserOrgsParams{
		Page:    &pageDefault,
		PerPage: &perPageDefault,

		Context: ctx,
	}
}

// NewGetV5UserOrgsParamsWithHTTPClient creates a new GetV5UserOrgsParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV5UserOrgsParamsWithHTTPClient(client *http.Client) *GetV5UserOrgsParams {
	var (
		pageDefault    = int32(1)
		perPageDefault = int32(20)
	)
	return &GetV5UserOrgsParams{
		Page:       &pageDefault,
		PerPage:    &perPageDefault,
		HTTPClient: client,
	}
}

/*GetV5UserOrgsParams contains all the parameters to send to the API endpoint
for the get v5 user orgs operation typically these are written to a http.Request
*/
type GetV5UserOrgsParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*Admin
	  只列出授权用户管理的组织

	*/
	Admin *bool
	/*Page
	  当前的页码

	*/
	Page *int32
	/*PerPage
	  每页的数量，最大为 100

	*/
	PerPage *int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v5 user orgs params
func (o *GetV5UserOrgsParams) WithTimeout(timeout time.Duration) *GetV5UserOrgsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v5 user orgs params
func (o *GetV5UserOrgsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v5 user orgs params
func (o *GetV5UserOrgsParams) WithContext(ctx context.Context) *GetV5UserOrgsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v5 user orgs params
func (o *GetV5UserOrgsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v5 user orgs params
func (o *GetV5UserOrgsParams) WithHTTPClient(client *http.Client) *GetV5UserOrgsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v5 user orgs params
func (o *GetV5UserOrgsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the get v5 user orgs params
func (o *GetV5UserOrgsParams) WithAccessToken(accessToken *string) *GetV5UserOrgsParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the get v5 user orgs params
func (o *GetV5UserOrgsParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithAdmin adds the admin to the get v5 user orgs params
func (o *GetV5UserOrgsParams) WithAdmin(admin *bool) *GetV5UserOrgsParams {
	o.SetAdmin(admin)
	return o
}

// SetAdmin adds the admin to the get v5 user orgs params
func (o *GetV5UserOrgsParams) SetAdmin(admin *bool) {
	o.Admin = admin
}

// WithPage adds the page to the get v5 user orgs params
func (o *GetV5UserOrgsParams) WithPage(page *int32) *GetV5UserOrgsParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v5 user orgs params
func (o *GetV5UserOrgsParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v5 user orgs params
func (o *GetV5UserOrgsParams) WithPerPage(perPage *int32) *GetV5UserOrgsParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v5 user orgs params
func (o *GetV5UserOrgsParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WriteToRequest writes these params to a swagger request
func (o *GetV5UserOrgsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// query param access_token
		var qrAccessToken string
		if o.AccessToken != nil {
			qrAccessToken = *o.AccessToken
		}
		qAccessToken := qrAccessToken
		if qAccessToken != "" {
			if err := r.SetQueryParam("access_token", qAccessToken); err != nil {
				return err
			}
		}

	}

	if o.Admin != nil {

		// query param admin
		var qrAdmin bool
		if o.Admin != nil {
			qrAdmin = *o.Admin
		}
		qAdmin := swag.FormatBool(qrAdmin)
		if qAdmin != "" {
			if err := r.SetQueryParam("admin", qAdmin); err != nil {
				return err
			}
		}

	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
