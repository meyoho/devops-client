// Code generated by go-swagger; DO NOT EDIT.

package repositories

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV5ReposOwnerRepoContentsPathParams creates a new PostV5ReposOwnerRepoContentsPathParams object
// with the default values initialized.
func NewPostV5ReposOwnerRepoContentsPathParams() *PostV5ReposOwnerRepoContentsPathParams {
	var ()
	return &PostV5ReposOwnerRepoContentsPathParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV5ReposOwnerRepoContentsPathParamsWithTimeout creates a new PostV5ReposOwnerRepoContentsPathParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV5ReposOwnerRepoContentsPathParamsWithTimeout(timeout time.Duration) *PostV5ReposOwnerRepoContentsPathParams {
	var ()
	return &PostV5ReposOwnerRepoContentsPathParams{

		timeout: timeout,
	}
}

// NewPostV5ReposOwnerRepoContentsPathParamsWithContext creates a new PostV5ReposOwnerRepoContentsPathParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV5ReposOwnerRepoContentsPathParamsWithContext(ctx context.Context) *PostV5ReposOwnerRepoContentsPathParams {
	var ()
	return &PostV5ReposOwnerRepoContentsPathParams{

		Context: ctx,
	}
}

// NewPostV5ReposOwnerRepoContentsPathParamsWithHTTPClient creates a new PostV5ReposOwnerRepoContentsPathParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV5ReposOwnerRepoContentsPathParamsWithHTTPClient(client *http.Client) *PostV5ReposOwnerRepoContentsPathParams {
	var ()
	return &PostV5ReposOwnerRepoContentsPathParams{
		HTTPClient: client,
	}
}

/*PostV5ReposOwnerRepoContentsPathParams contains all the parameters to send to the API endpoint
for the post v5 repos owner repo contents path operation typically these are written to a http.Request
*/
type PostV5ReposOwnerRepoContentsPathParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*AuthorEmail
	  Author的邮箱，默认为当前用户的邮箱

	*/
	AuthorEmail *string
	/*AuthorName
	  Author的名字，默认为当前用户的名字

	*/
	AuthorName *string
	/*Branch
	  分支名称。默认为仓库对默认分支

	*/
	Branch *string
	/*CommitterEmail
	  Committer的邮箱，默认为当前用户的邮箱

	*/
	CommitterEmail *string
	/*CommitterName
	  Committer的名字，默认为当前用户的名字

	*/
	CommitterName *string
	/*Content
	  文件内容, 要用 base64 编码

	*/
	Content string
	/*Message
	  提交信息

	*/
	Message string
	/*Owner
	  仓库所属空间地址(企业、组织或个人的地址path)

	*/
	Owner string
	/*Path
	  文件的路径

	*/
	Path string
	/*Repo
	  仓库路径(path)

	*/
	Repo string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithTimeout(timeout time.Duration) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithContext(ctx context.Context) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithHTTPClient(client *http.Client) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithAccessToken(accessToken *string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithAuthorEmail adds the authorEmail to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithAuthorEmail(authorEmail *string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetAuthorEmail(authorEmail)
	return o
}

// SetAuthorEmail adds the authorEmail to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetAuthorEmail(authorEmail *string) {
	o.AuthorEmail = authorEmail
}

// WithAuthorName adds the authorName to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithAuthorName(authorName *string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetAuthorName(authorName)
	return o
}

// SetAuthorName adds the authorName to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetAuthorName(authorName *string) {
	o.AuthorName = authorName
}

// WithBranch adds the branch to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithBranch(branch *string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetBranch(branch)
	return o
}

// SetBranch adds the branch to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetBranch(branch *string) {
	o.Branch = branch
}

// WithCommitterEmail adds the committerEmail to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithCommitterEmail(committerEmail *string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetCommitterEmail(committerEmail)
	return o
}

// SetCommitterEmail adds the committerEmail to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetCommitterEmail(committerEmail *string) {
	o.CommitterEmail = committerEmail
}

// WithCommitterName adds the committerName to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithCommitterName(committerName *string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetCommitterName(committerName)
	return o
}

// SetCommitterName adds the committerName to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetCommitterName(committerName *string) {
	o.CommitterName = committerName
}

// WithContent adds the content to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithContent(content string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetContent(content)
	return o
}

// SetContent adds the content to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetContent(content string) {
	o.Content = content
}

// WithMessage adds the message to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithMessage(message string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetMessage(message)
	return o
}

// SetMessage adds the message to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetMessage(message string) {
	o.Message = message
}

// WithOwner adds the owner to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithOwner(owner string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithPath adds the path to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithPath(path string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetPath(path)
	return o
}

// SetPath adds the path to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetPath(path string) {
	o.Path = path
}

// WithRepo adds the repo to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) WithRepo(repo string) *PostV5ReposOwnerRepoContentsPathParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the post v5 repos owner repo contents path params
func (o *PostV5ReposOwnerRepoContentsPathParams) SetRepo(repo string) {
	o.Repo = repo
}

// WriteToRequest writes these params to a swagger request
func (o *PostV5ReposOwnerRepoContentsPathParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// form param access_token
		var frAccessToken string
		if o.AccessToken != nil {
			frAccessToken = *o.AccessToken
		}
		fAccessToken := frAccessToken
		if fAccessToken != "" {
			if err := r.SetFormParam("access_token", fAccessToken); err != nil {
				return err
			}
		}

	}

	if o.AuthorEmail != nil {

		// form param author[email]
		var frAuthorEmail string
		if o.AuthorEmail != nil {
			frAuthorEmail = *o.AuthorEmail
		}
		fAuthorEmail := frAuthorEmail
		if fAuthorEmail != "" {
			if err := r.SetFormParam("author[email]", fAuthorEmail); err != nil {
				return err
			}
		}

	}

	if o.AuthorName != nil {

		// form param author[name]
		var frAuthorName string
		if o.AuthorName != nil {
			frAuthorName = *o.AuthorName
		}
		fAuthorName := frAuthorName
		if fAuthorName != "" {
			if err := r.SetFormParam("author[name]", fAuthorName); err != nil {
				return err
			}
		}

	}

	if o.Branch != nil {

		// form param branch
		var frBranch string
		if o.Branch != nil {
			frBranch = *o.Branch
		}
		fBranch := frBranch
		if fBranch != "" {
			if err := r.SetFormParam("branch", fBranch); err != nil {
				return err
			}
		}

	}

	if o.CommitterEmail != nil {

		// form param committer[email]
		var frCommitterEmail string
		if o.CommitterEmail != nil {
			frCommitterEmail = *o.CommitterEmail
		}
		fCommitterEmail := frCommitterEmail
		if fCommitterEmail != "" {
			if err := r.SetFormParam("committer[email]", fCommitterEmail); err != nil {
				return err
			}
		}

	}

	if o.CommitterName != nil {

		// form param committer[name]
		var frCommitterName string
		if o.CommitterName != nil {
			frCommitterName = *o.CommitterName
		}
		fCommitterName := frCommitterName
		if fCommitterName != "" {
			if err := r.SetFormParam("committer[name]", fCommitterName); err != nil {
				return err
			}
		}

	}

	// form param content
	frContent := o.Content
	fContent := frContent
	if fContent != "" {
		if err := r.SetFormParam("content", fContent); err != nil {
			return err
		}
	}

	// form param message
	frMessage := o.Message
	fMessage := frMessage
	if fMessage != "" {
		if err := r.SetFormParam("message", fMessage); err != nil {
			return err
		}
	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param path
	if err := r.SetPathParam("path", o.Path); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
