// Code generated by go-swagger; DO NOT EDIT.

package repositories

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewDeleteV5ReposOwnerRepoCommentsIDParams creates a new DeleteV5ReposOwnerRepoCommentsIDParams object
// with the default values initialized.
func NewDeleteV5ReposOwnerRepoCommentsIDParams() *DeleteV5ReposOwnerRepoCommentsIDParams {
	var ()
	return &DeleteV5ReposOwnerRepoCommentsIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewDeleteV5ReposOwnerRepoCommentsIDParamsWithTimeout creates a new DeleteV5ReposOwnerRepoCommentsIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewDeleteV5ReposOwnerRepoCommentsIDParamsWithTimeout(timeout time.Duration) *DeleteV5ReposOwnerRepoCommentsIDParams {
	var ()
	return &DeleteV5ReposOwnerRepoCommentsIDParams{

		timeout: timeout,
	}
}

// NewDeleteV5ReposOwnerRepoCommentsIDParamsWithContext creates a new DeleteV5ReposOwnerRepoCommentsIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewDeleteV5ReposOwnerRepoCommentsIDParamsWithContext(ctx context.Context) *DeleteV5ReposOwnerRepoCommentsIDParams {
	var ()
	return &DeleteV5ReposOwnerRepoCommentsIDParams{

		Context: ctx,
	}
}

// NewDeleteV5ReposOwnerRepoCommentsIDParamsWithHTTPClient creates a new DeleteV5ReposOwnerRepoCommentsIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewDeleteV5ReposOwnerRepoCommentsIDParamsWithHTTPClient(client *http.Client) *DeleteV5ReposOwnerRepoCommentsIDParams {
	var ()
	return &DeleteV5ReposOwnerRepoCommentsIDParams{
		HTTPClient: client,
	}
}

/*DeleteV5ReposOwnerRepoCommentsIDParams contains all the parameters to send to the API endpoint
for the delete v5 repos owner repo comments Id operation typically these are written to a http.Request
*/
type DeleteV5ReposOwnerRepoCommentsIDParams struct {

	/*AccessToken
	  用户授权码

	*/
	AccessToken *string
	/*ID
	  评论的ID

	*/
	ID int32
	/*Owner
	  仓库所属空间地址(企业、组织或个人的地址path)

	*/
	Owner string
	/*Repo
	  仓库路径(path)

	*/
	Repo string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) WithTimeout(timeout time.Duration) *DeleteV5ReposOwnerRepoCommentsIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) WithContext(ctx context.Context) *DeleteV5ReposOwnerRepoCommentsIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) WithHTTPClient(client *http.Client) *DeleteV5ReposOwnerRepoCommentsIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAccessToken adds the accessToken to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) WithAccessToken(accessToken *string) *DeleteV5ReposOwnerRepoCommentsIDParams {
	o.SetAccessToken(accessToken)
	return o
}

// SetAccessToken adds the accessToken to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) SetAccessToken(accessToken *string) {
	o.AccessToken = accessToken
}

// WithID adds the id to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) WithID(id int32) *DeleteV5ReposOwnerRepoCommentsIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) SetID(id int32) {
	o.ID = id
}

// WithOwner adds the owner to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) WithOwner(owner string) *DeleteV5ReposOwnerRepoCommentsIDParams {
	o.SetOwner(owner)
	return o
}

// SetOwner adds the owner to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) SetOwner(owner string) {
	o.Owner = owner
}

// WithRepo adds the repo to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) WithRepo(repo string) *DeleteV5ReposOwnerRepoCommentsIDParams {
	o.SetRepo(repo)
	return o
}

// SetRepo adds the repo to the delete v5 repos owner repo comments Id params
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) SetRepo(repo string) {
	o.Repo = repo
}

// WriteToRequest writes these params to a swagger request
func (o *DeleteV5ReposOwnerRepoCommentsIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.AccessToken != nil {

		// query param access_token
		var qrAccessToken string
		if o.AccessToken != nil {
			qrAccessToken = *o.AccessToken
		}
		qAccessToken := qrAccessToken
		if qAccessToken != "" {
			if err := r.SetQueryParam("access_token", qAccessToken); err != nil {
				return err
			}
		}

	}

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt32(o.ID)); err != nil {
		return err
	}

	// path param owner
	if err := r.SetPathParam("owner", o.Owner); err != nil {
		return err
	}

	// path param repo
	if err := r.SetPathParam("repo", o.Repo); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
