// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// EnterpriseMember 修改企业成员权限或备注
// swagger:model EnterpriseMember
type EnterpriseMember struct {

	// active
	Active string `json:"active,omitempty"`

	// enterprise
	Enterprise *EnterpriseBasic `json:"enterprise,omitempty"`

	// outsourced
	Outsourced string `json:"outsourced,omitempty"`

	// remark
	Remark string `json:"remark,omitempty"`

	// role
	Role string `json:"role,omitempty"`

	// url
	URL string `json:"url,omitempty"`

	// user
	User string `json:"user,omitempty"`
}

// Validate validates this enterprise member
func (m *EnterpriseMember) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateEnterprise(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *EnterpriseMember) validateEnterprise(formats strfmt.Registry) error {

	if swag.IsZero(m.Enterprise) { // not required
		return nil
	}

	if m.Enterprise != nil {
		if err := m.Enterprise.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("enterprise")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *EnterpriseMember) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *EnterpriseMember) UnmarshalBinary(b []byte) error {
	var res EnterpriseMember
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
