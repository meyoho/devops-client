package generator

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"sync"
	"text/template"

	devops "bitbucket.org/mathildetech/devops-client/pkg/api/v1"

	"github.com/go-swagger/go-swagger/cmd/swagger/commands/generate"
	"github.com/go-swagger/go-swagger/generator"
)

var (
	// registry devops tool registry
	registry map[string]string
	lock     sync.RWMutex
	once     sync.Once
)

func register(name, folder string) {
	once.Do(func() {
		registry = make(map[string]string)
	})
	lock.Lock()
	registry[name] = folder
	lock.Unlock()
}

func init() {
	tools := []string{
		devops.TypeHarbor,
		devops.TypeGithub,
		devops.TypeBitbucket,
		devops.TypeGitee,
		devops.TypeGitlab,
		devops.TypeConfluence,
		devops.TypeJira,
		devops.TypeTaiga,
		devops.TypeSonarQube,
		devops.TypeNexus,
		devops.TypeJenkins,
		devops.TypeGogs,
		devops.TypeGitea,
	}
	for _, name := range tools {
		register(name, fmt.Sprintf("artifacts/%s", name))
	}
}

type client struct {
	Name       string
	Version    string
	Models     []string
	Operations []string
	Spec       string
	Target     string
}

// Generate generates client
func Generate(name string) error {
	var err error
	if name != "" {
		folder, exist := registry[name]
		if !exist {
			return fmt.Errorf("tool %s not exist", name)
		}

		err = generateClient(extract(name, folder))
	} else {
		// generate all devops tool client
		for name, folder := range registry {
			err = generateClient(extract(name, folder))
			if err != nil {
				return err
			}
		}
	}

	return err
}

func extract(name, folder string) (clients []client) {
	dir, err := ioutil.ReadDir(folder)
	if err != nil {
		panic(err)
	}
	for _, f := range dir {
		if f.IsDir() {
			continue
		}
		version := strings.TrimSuffix(f.Name(), ".yaml")
		clients = append(clients, packClient(name, version))
	}

	return
}

func packClient(name, version string) client {
	return client{
		Name:    name,
		Version: version,
		Spec:    fmt.Sprintf("artifacts/%s/%s.yaml", name, version),
		Target:  fmt.Sprintf("pkg/%s/%s/", name, version),
	}
}

func generateClient(clients []client) error {
	for _, c := range clients {
		// clean folder
		err := ensureFolder(c.Target)
		if err != nil {
			log.Fatal(err)
		}
		// set opts
		fopt := &generate.FlattenCmdOptions{WithFlatten: []string{"minimal"}}
		opts := &generator.GenOpts{
			Name:              c.Name,
			Spec:              c.Spec,
			Target:            c.Target,
			APIPackage:        "operations",
			ModelPackage:      "models",
			ClientPackage:     "client",
			FlattenOpts:       fopt.SetFlattenOptions(nil),
			LanguageOpts:      generator.GoLangOpts(),
			IncludeModel:      true,
			IncludeValidator:  true,
			IncludeHandler:    true,
			IncludeParameters: true,
			IncludeResponses:  true,
			IncludeSupport:    true,
			IsClient:          true,
		}

		if err = opts.EnsureDefaults(); err != nil {
			log.Fatal(err)
		}

		if err = generator.GenerateClient(c.Name, c.Models, c.Operations, opts); err != nil {
			log.Fatal(err)
		}
		// generate adapter
		t, err := parseTemplate(c)

		adapterFile, err := os.OpenFile(fmt.Sprintf("%s/adapter.go", c.Target), os.O_RDWR|os.O_CREATE|os.O_APPEND, os.ModePerm)
		if err != nil {
			log.Fatal(err)
		}
		defer adapterFile.Close()

		if err = t.Execute(adapterFile, c); err != nil {
			log.Fatal(err)
		}

		log.Printf("Generate %s client to %s successfully\n", c.Name, c.Target)
	}

	return nil
}

func parseTemplate(c client) (t *template.Template, err error) {
	path := fmt.Sprintf("templates/%s-%s.tpl", c.Name, c.Version)
	if _, err = os.Stat(path); err != nil {
		path = fmt.Sprintf("templates/%s.tpl", c.Name)
	}

	t, err = template.ParseFiles(path)
	return
}

func ensureFolder(folder string) error {
	os.RemoveAll(folder)
	return os.MkdirAll(folder, os.ModePerm)
}
