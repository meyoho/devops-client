package pkg

import (
	devopsv1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v7 "bitbucket.org/mathildetech/devops-client/pkg/jira/v7"
)

func init() {
	register(devopsv1.TypeJira, versionOpt{
		version:   "v7",
		factory:   v7.NewClient(),
		isDefault: true,
	})
}
