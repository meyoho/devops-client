package nexus

import (
	"bitbucket.org/mathildetech/devops-client/pkg/cli/cmd/common"
	"github.com/spf13/cobra"
)

// NewNexusCommand create nexus command
func NewNexusCommand(commonOpts *common.Options) (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "nexus",
		Short: "nexus server",
	}

	cmd.AddCommand(NewScriptCommand(commonOpts),
		NewTaskCommand(commonOpts))
	return
}

type RunFunc func(cmd *cobra.Command, args []string)
