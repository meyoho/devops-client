package nexus_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
)

func TestTests(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("nexus_cmd.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "nexus_cmd_test", []Reporter{junitReporter})
}
