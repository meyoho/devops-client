package nexus

import (
	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/cli/cmd/common"
	"bitbucket.org/mathildetech/devops-client/pkg/cli/cmd/helper"
	"github.com/spf13/cobra"
	"io/ioutil"
)

// NewScriptCommand create script command
func NewScriptCommand(commonOpts *common.Options) (cmd *cobra.Command) {
	options := &ScriptOptions{
		Options: commonOpts,
	}

	cmd = &cobra.Command{
		Use: "script",
	}

	cmd.PersistentFlags().StringVarP(&options.Name, "name", "n", "",
		"The name of target script.")
	helper.MarkFlagRequired(cmd, "name")

	cmd.AddCommand(
		NewScriptAddCommand(options),
		NewScriptUpdateCommand(options),
		NewScriptRunCommand(options),
		NewScriptDeleteCommand(options),
		NewScriptListCommand(options),
		NewScriptGetCommand(options))
	return
}

// ScriptOptions represents the options for script command
type ScriptOptions struct {
	*common.Options

	Name    string
	Type    string
	Content string
	File    string
}

// GetNexusClient returns the client of nexus
func (o *ScriptOptions) GetNexusClient() (client v1.Interface, err error) {
	client, err = o.GetClient("nexus", "v3")
	return
}

func loadContentFromFile(options *ScriptOptions) RunFunc {
	return func(cmd *cobra.Command, args []string) {
		if options.File != "" {
			data, err := ioutil.ReadFile(options.File)
			if err == nil {
				options.Content = string(data)
			}
			helper.CheckErr(cmd, err)
		}
	}
}
