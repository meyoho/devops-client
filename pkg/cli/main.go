package main

import (
	"bitbucket.org/mathildetech/devops-client/pkg/cli/cmd"
)

func main() {
	cmd.Execute()
}
