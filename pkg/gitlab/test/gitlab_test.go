package test

import (
	"context"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	v4 "bitbucket.org/mathildetech/devops-client/pkg/gitlab/v4"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gopkg.in/h2non/gock.v1"
)

var _ = Describe("Devops Tool Gitlab tests", func() {
	var (
		// host
		host    = "https://gitlab.test"
		factory v1.ClientFactory
		client  v1.Interface
		opts    *v1.Options

		transport  = gock.NewTransport()
		httpClient = v1.NewDefaultClient()
	)
	// set gock transport for test
	httpClient.Transport = transport

	BeforeEach(func() {
		factory = v4.NewClient()
		opts = v1.NewOptions(v1.NewBasicConfig("gitlab.test", "", []string{}), v1.NewClient(httpClient), v1.NewTransport(transport))
		client = factory(opts)
	})

	AfterEach(func() {
		gock.Off()
	})

	BeforeSuite(func() {
		gock.InterceptClient(httpClient)
	})

	AfterSuite(func() {
		gock.OffAll()
		Expect(gock.IsDone()).To(Equal(true))
	})

	Context("GetRemoteRepos", func() {
		const (
			repos = `[
				{
					"id": 12818377,
					"description": "",
					"name": "test-repo-1",
					"name_with_namespace": "pwn2cy / test-repo-1",
					"path": "test-repo-1",
					"path_with_namespace": "pwn2cy/test-repo-1",
					"created_at": "2019-06-12T08:39:15.536Z",
					"default_branch": "master",
					"tag_list": [],
					"ssh_url_to_repo": "git@gitlab.com:pwn2cy/test-repo-1.git",
					"http_url_to_repo": "https://gitlab.com/pwn2cy/test-repo-1.git",
					"web_url": "https://gitlab.com/pwn2cy/test-repo-1",
					"readme_url": "https://gitlab.com/pwn2cy/test-repo-1/blob/master/README.md",
					"avatar_url": null,
					"star_count": 0,
					"forks_count": 0,
					"last_activity_at": "2019-06-17T10:18:03.568Z",
					"namespace": {
						"id": 5409933,
						"name": "pwn2cy",
						"path": "pwn2cy",
						"kind": "user",
						"full_path": "pwn2cy",
						"parent_id": null,
						"avatar_url": "https://secure.gravatar.com/avatar/e5bb8b84bc4febf506f64f4e9365cfad?s=80&d=identicon",
						"web_url": "https://gitlab.com/pwn2cy"
					},
					"_links": {
						"self": "https://gitlab.com/api/v4/projects/12818377",
						"issues": "https://gitlab.com/api/v4/projects/12818377/issues",
						"merge_requests": "https://gitlab.com/api/v4/projects/12818377/merge_requests",
						"repo_branches": "https://gitlab.com/api/v4/projects/12818377/repository/branches",
						"labels": "https://gitlab.com/api/v4/projects/12818377/labels",
						"events": "https://gitlab.com/api/v4/projects/12818377/events",
						"members": "https://gitlab.com/api/v4/projects/12818377/members"
					},
					"empty_repo": false,
					"archived": false,
					"visibility": "private",
					"owner": {
						"id": 4136301,
						"name": "pwn2cy",
						"username": "pwn2cy",
						"state": "active",
						"avatar_url": "https://secure.gravatar.com/avatar/e5bb8b84bc4febf506f64f4e9365cfad?s=80&d=identicon",
						"web_url": "https://gitlab.com/pwn2cy"
					},
					"resolve_outdated_diff_discussions": false,
					"container_registry_enabled": true,
					"issues_enabled": true,
					"merge_requests_enabled": true,
					"wiki_enabled": true,
					"jobs_enabled": true,
					"snippets_enabled": true,
					"shared_runners_enabled": true,
					"lfs_enabled": true,
					"creator_id": 4136301,
					"import_status": "none",
					"open_issues_count": 0,
					"ci_default_git_depth": null,
					"public_jobs": true,
					"ci_config_path": null,
					"shared_with_groups": [],
					"only_allow_merge_if_pipeline_succeeds": false,
					"request_access_enabled": false,
					"only_allow_merge_if_all_discussions_are_resolved": false,
					"printing_merge_request_link_enabled": true,
					"merge_method": "merge",
					"external_authorization_classification_label": "",
					"permissions": {
						"project_access": {
							"access_level": 40,
							"notification_level": 3
						},
						"group_access": null
					},
					"mirror": false
				}
			]`
		)

		BeforeEach(func() {
			gock.New(host).
				Get("/projects").
				MatchParam("membership", "true").
				Reply(200).
				JSON(repos)
		})

		It("list repos", func() {
			repos, err := client.GetRemoteRepos(context.Background())
			Expect(err).To(BeNil())
			Expect(len(repos.Owners)).To(Equal(1))
			Expect(repos.Owners[0].Name).To(Equal("pwn2cy"))
			Expect(len(repos.Owners[0].Repositories)).To(Equal(1))
			Expect(repos.Owners[0].Repositories[0].Name).To(Equal("test-repo-1"))
		})
	})

	Context("GetBranches", func() {
		const branches = `[
			{
				"name": "fix",
				"commit": {
					"id": "93cf1e0e0638b74ed307ca2c806a8169fa1745b7",
					"short_id": "93cf1e0e",
					"created_at": "2019-06-12T08:39:15.000+00:00",
					"parent_ids": null,
					"title": "Initial commit",
					"message": "Initial commit",
					"author_name": "pwn2cy",
					"author_email": "2779954012@qq.com",
					"authored_date": "2019-06-12T08:39:15.000+00:00",
					"committer_name": "pwn2cy",
					"committer_email": "2779954012@qq.com",
					"committed_date": "2019-06-12T08:39:15.000+00:00"
				},
				"merged": false,
				"protected": false,
				"developers_can_push": false,
				"developers_can_merge": false,
				"can_push": true,
				"default": false
			},
			{
				"name": "master",
				"commit": {
					"id": "93cf1e0e0638b74ed307ca2c806a8169fa1745b7",
					"short_id": "93cf1e0e",
					"created_at": "2019-06-12T08:39:15.000+00:00",
					"parent_ids": null,
					"title": "Initial commit",
					"message": "Initial commit",
					"author_name": "pwn2cy",
					"author_email": "2779954012@qq.com",
					"authored_date": "2019-06-12T08:39:15.000+00:00",
					"committer_name": "pwn2cy",
					"committer_email": "2779954012@qq.com",
					"committed_date": "2019-06-12T08:39:15.000+00:00"
				},
				"merged": false,
				"protected": true,
				"developers_can_push": false,
				"developers_can_merge": false,
				"can_push": true,
				"default": true
			}
		]`
		BeforeEach(func() {
			gock.New(host).
				Get("branches").
				Reply(200).
				JSON(branches)
		})

		It("get repo branches", func() {
			branches, err := client.GetBranches(context.Background(), "pwn2cy", "test-repo-1", "pwn2cy/test-repo-1")
			Expect(err).To(BeNil())
			Expect(len(branches)).To(Equal(2))
			Expect(branches[0].Name).To(Equal("fix"))
			Expect(branches[0].Commit).To(Equal("93cf1e0e0638b74ed307ca2c806a8169fa1745b7"))
		})
	})

	Context("CreateCodeRepoProject", func() {
		const (
			org = `{
				"id": 5467191,
				"web_url": "https://gitlab.com/groups/group-cyalauda-2",
				"name": "group-cyalauda-2",
				"path": "group-cyalauda-2",
				"description": "",
				"visibility": "private",
				"lfs_enabled": true,
				"avatar_url": null,
				"request_access_enabled": false,
				"full_name": "group-cyalauda-2",
				"full_path": "group-cyalauda-2",
				"parent_id": null,
				"projects": [],
				"shared_projects": [],
				"ldap_cn": null,
				"ldap_access": null,
				"shared_runners_minutes_limit": null,
				"extra_shared_runners_minutes_limit": null
			}`
		)

		BeforeEach(func() {
			gock.New(host).
				Post("/groups").
				Reply(201).
				JSON(org)
		})

		It("create group", func() {
			org, err := client.CreateCodeRepoProject(context.Background(), v1.CreateProjectOptions{Name: "group-cyalauda-2"})
			Expect(err).To(BeNil())
			Expect(org.Name).To(Equal("group-cyalauda-2"))
		})
	})

	Context("ListCodeRepoProjects", func() {
		const (
			orgs = `[
				{
					"id": 5446964,
					"web_url": "https://gitlab.com/groups/group-cyalauda-1",
					"name": "group-cyalauda-1",
					"path": "group-cyalauda-1",
					"description": "",
					"visibility": "private",
					"lfs_enabled": true,
					"avatar_url": null,
					"request_access_enabled": false,
					"full_name": "group-cyalauda-1",
					"full_path": "group-cyalauda-1",
					"parent_id": null,
					"ldap_cn": null,
					"ldap_access": null
				}
			]`

			user = `{
				  "id": 1,
				  "username": "u1",
				  "name": "John Smith",
				  "state": "active",
				  "avatar_url": "http://localhost:3000/uploads/user/avatar/1/cd8.jpeg",
				  "web_url": "http://localhost:3000/john_smith",
				  "created_at": "2012-05-23T08:00:58Z",
				  "bio": null,
				  "location": null,
				  "email": "u1@example.com",
				  "public_email": "u1@example.com",
				  "skype": "",
				  "linkedin": "",
				  "twitter": "",
				  "website_url": "",
				  "organization": ""
				}`
		)
		BeforeEach(func() {
			gock.New(host).
				Get("/groups").
				Reply(200).
				JSON(orgs)

			gock.New(host).
				Get("/user").
				Reply(200).
				JSON(user)
		})

		It("list groups", func() {
			groups, err := client.ListCodeRepoProjects(context.Background(), v1.ListProjectOptions{})
			Expect(err).To(BeNil())
			Expect(groups.Items).To(HaveLen(2))
			Expect(groups.Items[0].Name).To(Equal("group-cyalauda-1"))
		})
	})

})
