// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// PipelineBasic pipeline basic
// swagger:model PipelineBasic
type PipelineBasic struct {

	// id
	ID string `json:"id,omitempty"`

	// ref
	Ref string `json:"ref,omitempty"`

	// sha
	Sha string `json:"sha,omitempty"`

	// status
	Status string `json:"status,omitempty"`
}

// Validate validates this pipeline basic
func (m *PipelineBasic) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *PipelineBasic) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *PipelineBasic) UnmarshalBinary(b []byte) error {
	var res PipelineBasic
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
