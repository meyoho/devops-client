// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// RepoDiff repo diff
// swagger:model RepoDiff
type RepoDiff struct {

	// a mode
	AMode string `json:"a_mode,omitempty"`

	// b mode
	BMode string `json:"b_mode,omitempty"`

	// deleted file
	DeletedFile string `json:"deleted_file,omitempty"`

	// diff
	Diff string `json:"diff,omitempty"`

	// new file
	NewFile string `json:"new_file,omitempty"`

	// new path
	NewPath string `json:"new_path,omitempty"`

	// old path
	OldPath string `json:"old_path,omitempty"`

	// renamed file
	RenamedFile string `json:"renamed_file,omitempty"`
}

// Validate validates this repo diff
func (m *RepoDiff) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *RepoDiff) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *RepoDiff) UnmarshalBinary(b []byte) error {
	var res RepoDiff
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
