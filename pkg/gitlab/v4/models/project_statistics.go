// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// ProjectStatistics project statistics
// swagger:model ProjectStatistics
type ProjectStatistics struct {

	// build artifacts size
	BuildArtifactsSize int64 `json:"build_artifacts_size,omitempty"`

	// commit count
	CommitCount int64 `json:"commit_count,omitempty"`

	// lfs objects size
	LfsObjectsSize int64 `json:"lfs_objects_size,omitempty"`

	// repository size
	RepositorySize int64 `json:"repository_size,omitempty"`

	// storage size
	StorageSize int64 `json:"storage_size,omitempty"`
}

// Validate validates this project statistics
func (m *ProjectStatistics) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *ProjectStatistics) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *ProjectStatistics) UnmarshalBinary(b []byte) error {
	var res ProjectStatistics
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
