// Code generated by go-swagger; DO NOT EDIT.

package licenses

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new licenses API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for licenses API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
GetV4Licenses gets the list of the available license template

This feature was introduced in GitLab 8.7. This endpoint is deprecated and will be removed in GitLab 9.0.
*/
func (a *Client) GetV4Licenses(params *GetV4LicensesParams, authInfo runtime.ClientAuthInfoWriter) (*GetV4LicensesOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetV4LicensesParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getV4Licenses",
		Method:             "GET",
		PathPattern:        "/v4/licenses",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &GetV4LicensesReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetV4LicensesOK), nil

}

/*
GetV4LicensesName gets the text for a specific license

This feature was introduced in GitLab 8.7. This endpoint is deprecated and will be removed in GitLab 9.0.
*/
func (a *Client) GetV4LicensesName(params *GetV4LicensesNameParams, authInfo runtime.ClientAuthInfoWriter) (*GetV4LicensesNameOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewGetV4LicensesNameParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "getV4LicensesName",
		Method:             "GET",
		PathPattern:        "/v4/licenses/{name}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{""},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &GetV4LicensesNameReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	return result.(*GetV4LicensesNameOK), nil

}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
