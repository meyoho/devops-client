// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams creates a new GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams object
// with the default values initialized.
func NewGetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams() *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams {
	var ()
	return &GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParamsWithTimeout creates a new GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParamsWithTimeout(timeout time.Duration) *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams {
	var ()
	return &GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams{

		timeout: timeout,
	}
}

// NewGetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParamsWithContext creates a new GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParamsWithContext(ctx context.Context) *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams {
	var ()
	return &GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams{

		Context: ctx,
	}
}

// NewGetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParamsWithHTTPClient creates a new GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParamsWithHTTPClient(client *http.Client) *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams {
	var ()
	return &GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams{
		HTTPClient: client,
	}
}

/*GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams contains all the parameters to send to the API endpoint
for the get v4 projects Id snippets snippet Id award emoji award Id operation typically these are written to a http.Request
*/
type GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams struct {

	/*AwardID
	  The ID of the award

	*/
	AwardID int32
	/*ID*/
	ID int32
	/*SnippetID*/
	SnippetID int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) WithTimeout(timeout time.Duration) *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) WithContext(ctx context.Context) *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) WithHTTPClient(client *http.Client) *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithAwardID adds the awardID to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) WithAwardID(awardID int32) *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams {
	o.SetAwardID(awardID)
	return o
}

// SetAwardID adds the awardId to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) SetAwardID(awardID int32) {
	o.AwardID = awardID
}

// WithID adds the id to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) WithID(id int32) *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) SetID(id int32) {
	o.ID = id
}

// WithSnippetID adds the snippetID to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) WithSnippetID(snippetID int32) *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams {
	o.SetSnippetID(snippetID)
	return o
}

// SetSnippetID adds the snippetId to the get v4 projects Id snippets snippet Id award emoji award Id params
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) SetSnippetID(snippetID int32) {
	o.SnippetID = snippetID
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsIDSnippetsSnippetIDAwardEmojiAwardIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param award_id
	if err := r.SetPathParam("award_id", swag.FormatInt32(o.AwardID)); err != nil {
		return err
	}

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt32(o.ID)); err != nil {
		return err
	}

	// path param snippet_id
	if err := r.SetPathParam("snippet_id", swag.FormatInt32(o.SnippetID)); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
