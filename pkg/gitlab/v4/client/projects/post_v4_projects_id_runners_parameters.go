// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV4ProjectsIDRunnersParams creates a new PostV4ProjectsIDRunnersParams object
// with the default values initialized.
func NewPostV4ProjectsIDRunnersParams() *PostV4ProjectsIDRunnersParams {
	var ()
	return &PostV4ProjectsIDRunnersParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV4ProjectsIDRunnersParamsWithTimeout creates a new PostV4ProjectsIDRunnersParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV4ProjectsIDRunnersParamsWithTimeout(timeout time.Duration) *PostV4ProjectsIDRunnersParams {
	var ()
	return &PostV4ProjectsIDRunnersParams{

		timeout: timeout,
	}
}

// NewPostV4ProjectsIDRunnersParamsWithContext creates a new PostV4ProjectsIDRunnersParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV4ProjectsIDRunnersParamsWithContext(ctx context.Context) *PostV4ProjectsIDRunnersParams {
	var ()
	return &PostV4ProjectsIDRunnersParams{

		Context: ctx,
	}
}

// NewPostV4ProjectsIDRunnersParamsWithHTTPClient creates a new PostV4ProjectsIDRunnersParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV4ProjectsIDRunnersParamsWithHTTPClient(client *http.Client) *PostV4ProjectsIDRunnersParams {
	var ()
	return &PostV4ProjectsIDRunnersParams{
		HTTPClient: client,
	}
}

/*PostV4ProjectsIDRunnersParams contains all the parameters to send to the API endpoint
for the post v4 projects Id runners operation typically these are written to a http.Request
*/
type PostV4ProjectsIDRunnersParams struct {

	/*ID
	  The ID of a project

	*/
	ID string
	/*RunnerID
	  The ID of the runner

	*/
	RunnerID int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v4 projects Id runners params
func (o *PostV4ProjectsIDRunnersParams) WithTimeout(timeout time.Duration) *PostV4ProjectsIDRunnersParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v4 projects Id runners params
func (o *PostV4ProjectsIDRunnersParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v4 projects Id runners params
func (o *PostV4ProjectsIDRunnersParams) WithContext(ctx context.Context) *PostV4ProjectsIDRunnersParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v4 projects Id runners params
func (o *PostV4ProjectsIDRunnersParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v4 projects Id runners params
func (o *PostV4ProjectsIDRunnersParams) WithHTTPClient(client *http.Client) *PostV4ProjectsIDRunnersParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v4 projects Id runners params
func (o *PostV4ProjectsIDRunnersParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the post v4 projects Id runners params
func (o *PostV4ProjectsIDRunnersParams) WithID(id string) *PostV4ProjectsIDRunnersParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the post v4 projects Id runners params
func (o *PostV4ProjectsIDRunnersParams) SetID(id string) {
	o.ID = id
}

// WithRunnerID adds the runnerID to the post v4 projects Id runners params
func (o *PostV4ProjectsIDRunnersParams) WithRunnerID(runnerID int32) *PostV4ProjectsIDRunnersParams {
	o.SetRunnerID(runnerID)
	return o
}

// SetRunnerID adds the runnerId to the post v4 projects Id runners params
func (o *PostV4ProjectsIDRunnersParams) SetRunnerID(runnerID int32) {
	o.RunnerID = runnerID
}

// WriteToRequest writes these params to a swagger request
func (o *PostV4ProjectsIDRunnersParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	// form param runner_id
	frRunnerID := o.RunnerID
	fRunnerID := swag.FormatInt32(frRunnerID)
	if fRunnerID != "" {
		if err := r.SetFormParam("runner_id", fRunnerID); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
