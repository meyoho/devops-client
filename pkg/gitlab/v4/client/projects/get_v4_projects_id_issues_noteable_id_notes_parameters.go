// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewGetV4ProjectsIDIssuesNoteableIDNotesParams creates a new GetV4ProjectsIDIssuesNoteableIDNotesParams object
// with the default values initialized.
func NewGetV4ProjectsIDIssuesNoteableIDNotesParams() *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	var ()
	return &GetV4ProjectsIDIssuesNoteableIDNotesParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewGetV4ProjectsIDIssuesNoteableIDNotesParamsWithTimeout creates a new GetV4ProjectsIDIssuesNoteableIDNotesParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewGetV4ProjectsIDIssuesNoteableIDNotesParamsWithTimeout(timeout time.Duration) *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	var ()
	return &GetV4ProjectsIDIssuesNoteableIDNotesParams{

		timeout: timeout,
	}
}

// NewGetV4ProjectsIDIssuesNoteableIDNotesParamsWithContext creates a new GetV4ProjectsIDIssuesNoteableIDNotesParams object
// with the default values initialized, and the ability to set a context for a request
func NewGetV4ProjectsIDIssuesNoteableIDNotesParamsWithContext(ctx context.Context) *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	var ()
	return &GetV4ProjectsIDIssuesNoteableIDNotesParams{

		Context: ctx,
	}
}

// NewGetV4ProjectsIDIssuesNoteableIDNotesParamsWithHTTPClient creates a new GetV4ProjectsIDIssuesNoteableIDNotesParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewGetV4ProjectsIDIssuesNoteableIDNotesParamsWithHTTPClient(client *http.Client) *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	var ()
	return &GetV4ProjectsIDIssuesNoteableIDNotesParams{
		HTTPClient: client,
	}
}

/*GetV4ProjectsIDIssuesNoteableIDNotesParams contains all the parameters to send to the API endpoint
for the get v4 projects Id issues noteable Id notes operation typically these are written to a http.Request
*/
type GetV4ProjectsIDIssuesNoteableIDNotesParams struct {

	/*ID
	  The ID of a project

	*/
	ID string
	/*NoteableID
	  The ID of the noteable

	*/
	NoteableID int32
	/*Page
	  Current page number

	*/
	Page *int32
	/*PerPage
	  Number of items per page

	*/
	PerPage *int32

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) WithTimeout(timeout time.Duration) *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) WithContext(ctx context.Context) *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) WithHTTPClient(client *http.Client) *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) WithID(id string) *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) SetID(id string) {
	o.ID = id
}

// WithNoteableID adds the noteableID to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) WithNoteableID(noteableID int32) *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetNoteableID(noteableID)
	return o
}

// SetNoteableID adds the noteableId to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) SetNoteableID(noteableID int32) {
	o.NoteableID = noteableID
}

// WithPage adds the page to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) WithPage(page *int32) *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetPage(page)
	return o
}

// SetPage adds the page to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) SetPage(page *int32) {
	o.Page = page
}

// WithPerPage adds the perPage to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) WithPerPage(perPage *int32) *GetV4ProjectsIDIssuesNoteableIDNotesParams {
	o.SetPerPage(perPage)
	return o
}

// SetPerPage adds the perPage to the get v4 projects Id issues noteable Id notes params
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) SetPerPage(perPage *int32) {
	o.PerPage = perPage
}

// WriteToRequest writes these params to a swagger request
func (o *GetV4ProjectsIDIssuesNoteableIDNotesParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	// path param noteable_id
	if err := r.SetPathParam("noteable_id", swag.FormatInt32(o.NoteableID)); err != nil {
		return err
	}

	if o.Page != nil {

		// query param page
		var qrPage int32
		if o.Page != nil {
			qrPage = *o.Page
		}
		qPage := swag.FormatInt32(qrPage)
		if qPage != "" {
			if err := r.SetQueryParam("page", qPage); err != nil {
				return err
			}
		}

	}

	if o.PerPage != nil {

		// query param per_page
		var qrPerPage int32
		if o.PerPage != nil {
			qrPerPage = *o.PerPage
		}
		qPerPage := swag.FormatInt32(qrPerPage)
		if qPerPage != "" {
			if err := r.SetQueryParam("per_page", qPerPage); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
