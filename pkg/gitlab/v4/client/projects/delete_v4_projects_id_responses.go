// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// DeleteV4ProjectsIDReader is a Reader for the DeleteV4ProjectsID structure.
type DeleteV4ProjectsIDReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *DeleteV4ProjectsIDReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {

	case 204:
		result := NewDeleteV4ProjectsIDNoContent()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil

	default:
		return nil, runtime.NewAPIError("unknown error", response, response.Code())
	}
}

// NewDeleteV4ProjectsIDNoContent creates a DeleteV4ProjectsIDNoContent with default headers values
func NewDeleteV4ProjectsIDNoContent() *DeleteV4ProjectsIDNoContent {
	return &DeleteV4ProjectsIDNoContent{}
}

/*DeleteV4ProjectsIDNoContent handles this case with default header values.

Remove a project
*/
type DeleteV4ProjectsIDNoContent struct {
}

func (o *DeleteV4ProjectsIDNoContent) Error() string {
	return fmt.Sprintf("[DELETE /v4/projects/{id}][%d] deleteV4ProjectsIdNoContent ", 204)
}

func (o *DeleteV4ProjectsIDNoContent) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	return nil
}
