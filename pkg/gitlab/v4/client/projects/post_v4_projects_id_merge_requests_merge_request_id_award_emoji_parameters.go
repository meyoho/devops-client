// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams creates a new PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams object
// with the default values initialized.
func NewPostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams() *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams {
	var ()
	return &PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParamsWithTimeout creates a new PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParamsWithTimeout(timeout time.Duration) *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams {
	var ()
	return &PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams{

		timeout: timeout,
	}
}

// NewPostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParamsWithContext creates a new PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams object
// with the default values initialized, and the ability to set a context for a request
func NewPostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParamsWithContext(ctx context.Context) *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams {
	var ()
	return &PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams{

		Context: ctx,
	}
}

// NewPostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParamsWithHTTPClient creates a new PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParamsWithHTTPClient(client *http.Client) *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams {
	var ()
	return &PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams{
		HTTPClient: client,
	}
}

/*PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams contains all the parameters to send to the API endpoint
for the post v4 projects Id merge requests merge request Id award emoji operation typically these are written to a http.Request
*/
type PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams struct {

	/*ID*/
	ID int32
	/*MergeRequestID*/
	MergeRequestID int32
	/*Name
	  The name of a award_emoji (without colons)

	*/
	Name string

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) WithTimeout(timeout time.Duration) *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) WithContext(ctx context.Context) *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) WithHTTPClient(client *http.Client) *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithID adds the id to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) WithID(id int32) *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) SetID(id int32) {
	o.ID = id
}

// WithMergeRequestID adds the mergeRequestID to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) WithMergeRequestID(mergeRequestID int32) *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams {
	o.SetMergeRequestID(mergeRequestID)
	return o
}

// SetMergeRequestID adds the mergeRequestId to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) SetMergeRequestID(mergeRequestID int32) {
	o.MergeRequestID = mergeRequestID
}

// WithName adds the name to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) WithName(name string) *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams {
	o.SetName(name)
	return o
}

// SetName adds the name to the post v4 projects Id merge requests merge request Id award emoji params
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) SetName(name string) {
	o.Name = name
}

// WriteToRequest writes these params to a swagger request
func (o *PostV4ProjectsIDMergeRequestsMergeRequestIDAwardEmojiParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	// path param id
	if err := r.SetPathParam("id", swag.FormatInt32(o.ID)); err != nil {
		return err
	}

	// path param merge_request_id
	if err := r.SetPathParam("merge_request_id", swag.FormatInt32(o.MergeRequestID)); err != nil {
		return err
	}

	// form param name
	frName := o.Name
	fName := frName
	if fName != "" {
		if err := r.SetFormParam("name", fName); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
