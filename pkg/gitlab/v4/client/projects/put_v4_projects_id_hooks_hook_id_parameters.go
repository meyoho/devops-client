// Code generated by go-swagger; DO NOT EDIT.

package projects

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/swag"

	strfmt "github.com/go-openapi/strfmt"
)

// NewPutV4ProjectsIDHooksHookIDParams creates a new PutV4ProjectsIDHooksHookIDParams object
// with the default values initialized.
func NewPutV4ProjectsIDHooksHookIDParams() *PutV4ProjectsIDHooksHookIDParams {
	var ()
	return &PutV4ProjectsIDHooksHookIDParams{

		timeout: cr.DefaultTimeout,
	}
}

// NewPutV4ProjectsIDHooksHookIDParamsWithTimeout creates a new PutV4ProjectsIDHooksHookIDParams object
// with the default values initialized, and the ability to set a timeout on a request
func NewPutV4ProjectsIDHooksHookIDParamsWithTimeout(timeout time.Duration) *PutV4ProjectsIDHooksHookIDParams {
	var ()
	return &PutV4ProjectsIDHooksHookIDParams{

		timeout: timeout,
	}
}

// NewPutV4ProjectsIDHooksHookIDParamsWithContext creates a new PutV4ProjectsIDHooksHookIDParams object
// with the default values initialized, and the ability to set a context for a request
func NewPutV4ProjectsIDHooksHookIDParamsWithContext(ctx context.Context) *PutV4ProjectsIDHooksHookIDParams {
	var ()
	return &PutV4ProjectsIDHooksHookIDParams{

		Context: ctx,
	}
}

// NewPutV4ProjectsIDHooksHookIDParamsWithHTTPClient creates a new PutV4ProjectsIDHooksHookIDParams object
// with the default values initialized, and the ability to set a custom HTTPClient for a request
func NewPutV4ProjectsIDHooksHookIDParamsWithHTTPClient(client *http.Client) *PutV4ProjectsIDHooksHookIDParams {
	var ()
	return &PutV4ProjectsIDHooksHookIDParams{
		HTTPClient: client,
	}
}

/*PutV4ProjectsIDHooksHookIDParams contains all the parameters to send to the API endpoint
for the put v4 projects Id hooks hook Id operation typically these are written to a http.Request
*/
type PutV4ProjectsIDHooksHookIDParams struct {

	/*BuildEvents
	  Trigger hook on build events

	*/
	BuildEvents *bool
	/*EnableSslVerification
	  Do SSL verification when triggering the hook

	*/
	EnableSslVerification *bool
	/*HookID
	  The ID of the hook to update

	*/
	HookID int32
	/*ID
	  The ID of a project

	*/
	ID string
	/*IssuesEvents
	  Trigger hook on issues events

	*/
	IssuesEvents *bool
	/*MergeRequestsEvents
	  Trigger hook on merge request events

	*/
	MergeRequestsEvents *bool
	/*NoteEvents
	  Trigger hook on note(comment) events

	*/
	NoteEvents *bool
	/*PipelineEvents
	  Trigger hook on pipeline events

	*/
	PipelineEvents *bool
	/*PushEvents
	  Trigger hook on push events

	*/
	PushEvents *bool
	/*TagPushEvents
	  Trigger hook on tag push events

	*/
	TagPushEvents *bool
	/*Token
	  Secret token to validate received payloads; this will not be returned in the response

	*/
	Token *string
	/*URL
	  The URL to send the request to

	*/
	URL string
	/*WikiPageEvents
	  Trigger hook on wiki events

	*/
	WikiPageEvents *bool

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithTimeout adds the timeout to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithTimeout(timeout time.Duration) *PutV4ProjectsIDHooksHookIDParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithContext(ctx context.Context) *PutV4ProjectsIDHooksHookIDParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithHTTPClient(client *http.Client) *PutV4ProjectsIDHooksHookIDParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBuildEvents adds the buildEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithBuildEvents(buildEvents *bool) *PutV4ProjectsIDHooksHookIDParams {
	o.SetBuildEvents(buildEvents)
	return o
}

// SetBuildEvents adds the buildEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetBuildEvents(buildEvents *bool) {
	o.BuildEvents = buildEvents
}

// WithEnableSslVerification adds the enableSslVerification to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithEnableSslVerification(enableSslVerification *bool) *PutV4ProjectsIDHooksHookIDParams {
	o.SetEnableSslVerification(enableSslVerification)
	return o
}

// SetEnableSslVerification adds the enableSslVerification to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetEnableSslVerification(enableSslVerification *bool) {
	o.EnableSslVerification = enableSslVerification
}

// WithHookID adds the hookID to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithHookID(hookID int32) *PutV4ProjectsIDHooksHookIDParams {
	o.SetHookID(hookID)
	return o
}

// SetHookID adds the hookId to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetHookID(hookID int32) {
	o.HookID = hookID
}

// WithID adds the id to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithID(id string) *PutV4ProjectsIDHooksHookIDParams {
	o.SetID(id)
	return o
}

// SetID adds the id to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetID(id string) {
	o.ID = id
}

// WithIssuesEvents adds the issuesEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithIssuesEvents(issuesEvents *bool) *PutV4ProjectsIDHooksHookIDParams {
	o.SetIssuesEvents(issuesEvents)
	return o
}

// SetIssuesEvents adds the issuesEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetIssuesEvents(issuesEvents *bool) {
	o.IssuesEvents = issuesEvents
}

// WithMergeRequestsEvents adds the mergeRequestsEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithMergeRequestsEvents(mergeRequestsEvents *bool) *PutV4ProjectsIDHooksHookIDParams {
	o.SetMergeRequestsEvents(mergeRequestsEvents)
	return o
}

// SetMergeRequestsEvents adds the mergeRequestsEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetMergeRequestsEvents(mergeRequestsEvents *bool) {
	o.MergeRequestsEvents = mergeRequestsEvents
}

// WithNoteEvents adds the noteEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithNoteEvents(noteEvents *bool) *PutV4ProjectsIDHooksHookIDParams {
	o.SetNoteEvents(noteEvents)
	return o
}

// SetNoteEvents adds the noteEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetNoteEvents(noteEvents *bool) {
	o.NoteEvents = noteEvents
}

// WithPipelineEvents adds the pipelineEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithPipelineEvents(pipelineEvents *bool) *PutV4ProjectsIDHooksHookIDParams {
	o.SetPipelineEvents(pipelineEvents)
	return o
}

// SetPipelineEvents adds the pipelineEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetPipelineEvents(pipelineEvents *bool) {
	o.PipelineEvents = pipelineEvents
}

// WithPushEvents adds the pushEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithPushEvents(pushEvents *bool) *PutV4ProjectsIDHooksHookIDParams {
	o.SetPushEvents(pushEvents)
	return o
}

// SetPushEvents adds the pushEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetPushEvents(pushEvents *bool) {
	o.PushEvents = pushEvents
}

// WithTagPushEvents adds the tagPushEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithTagPushEvents(tagPushEvents *bool) *PutV4ProjectsIDHooksHookIDParams {
	o.SetTagPushEvents(tagPushEvents)
	return o
}

// SetTagPushEvents adds the tagPushEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetTagPushEvents(tagPushEvents *bool) {
	o.TagPushEvents = tagPushEvents
}

// WithToken adds the token to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithToken(token *string) *PutV4ProjectsIDHooksHookIDParams {
	o.SetToken(token)
	return o
}

// SetToken adds the token to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetToken(token *string) {
	o.Token = token
}

// WithURL adds the url to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithURL(url string) *PutV4ProjectsIDHooksHookIDParams {
	o.SetURL(url)
	return o
}

// SetURL adds the url to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetURL(url string) {
	o.URL = url
}

// WithWikiPageEvents adds the wikiPageEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) WithWikiPageEvents(wikiPageEvents *bool) *PutV4ProjectsIDHooksHookIDParams {
	o.SetWikiPageEvents(wikiPageEvents)
	return o
}

// SetWikiPageEvents adds the wikiPageEvents to the put v4 projects Id hooks hook Id params
func (o *PutV4ProjectsIDHooksHookIDParams) SetWikiPageEvents(wikiPageEvents *bool) {
	o.WikiPageEvents = wikiPageEvents
}

// WriteToRequest writes these params to a swagger request
func (o *PutV4ProjectsIDHooksHookIDParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error

	if o.BuildEvents != nil {

		// form param build_events
		var frBuildEvents bool
		if o.BuildEvents != nil {
			frBuildEvents = *o.BuildEvents
		}
		fBuildEvents := swag.FormatBool(frBuildEvents)
		if fBuildEvents != "" {
			if err := r.SetFormParam("build_events", fBuildEvents); err != nil {
				return err
			}
		}

	}

	if o.EnableSslVerification != nil {

		// form param enable_ssl_verification
		var frEnableSslVerification bool
		if o.EnableSslVerification != nil {
			frEnableSslVerification = *o.EnableSslVerification
		}
		fEnableSslVerification := swag.FormatBool(frEnableSslVerification)
		if fEnableSslVerification != "" {
			if err := r.SetFormParam("enable_ssl_verification", fEnableSslVerification); err != nil {
				return err
			}
		}

	}

	// path param hook_id
	if err := r.SetPathParam("hook_id", swag.FormatInt32(o.HookID)); err != nil {
		return err
	}

	// path param id
	if err := r.SetPathParam("id", o.ID); err != nil {
		return err
	}

	if o.IssuesEvents != nil {

		// form param issues_events
		var frIssuesEvents bool
		if o.IssuesEvents != nil {
			frIssuesEvents = *o.IssuesEvents
		}
		fIssuesEvents := swag.FormatBool(frIssuesEvents)
		if fIssuesEvents != "" {
			if err := r.SetFormParam("issues_events", fIssuesEvents); err != nil {
				return err
			}
		}

	}

	if o.MergeRequestsEvents != nil {

		// form param merge_requests_events
		var frMergeRequestsEvents bool
		if o.MergeRequestsEvents != nil {
			frMergeRequestsEvents = *o.MergeRequestsEvents
		}
		fMergeRequestsEvents := swag.FormatBool(frMergeRequestsEvents)
		if fMergeRequestsEvents != "" {
			if err := r.SetFormParam("merge_requests_events", fMergeRequestsEvents); err != nil {
				return err
			}
		}

	}

	if o.NoteEvents != nil {

		// form param note_events
		var frNoteEvents bool
		if o.NoteEvents != nil {
			frNoteEvents = *o.NoteEvents
		}
		fNoteEvents := swag.FormatBool(frNoteEvents)
		if fNoteEvents != "" {
			if err := r.SetFormParam("note_events", fNoteEvents); err != nil {
				return err
			}
		}

	}

	if o.PipelineEvents != nil {

		// form param pipeline_events
		var frPipelineEvents bool
		if o.PipelineEvents != nil {
			frPipelineEvents = *o.PipelineEvents
		}
		fPipelineEvents := swag.FormatBool(frPipelineEvents)
		if fPipelineEvents != "" {
			if err := r.SetFormParam("pipeline_events", fPipelineEvents); err != nil {
				return err
			}
		}

	}

	if o.PushEvents != nil {

		// form param push_events
		var frPushEvents bool
		if o.PushEvents != nil {
			frPushEvents = *o.PushEvents
		}
		fPushEvents := swag.FormatBool(frPushEvents)
		if fPushEvents != "" {
			if err := r.SetFormParam("push_events", fPushEvents); err != nil {
				return err
			}
		}

	}

	if o.TagPushEvents != nil {

		// form param tag_push_events
		var frTagPushEvents bool
		if o.TagPushEvents != nil {
			frTagPushEvents = *o.TagPushEvents
		}
		fTagPushEvents := swag.FormatBool(frTagPushEvents)
		if fTagPushEvents != "" {
			if err := r.SetFormParam("tag_push_events", fTagPushEvents); err != nil {
				return err
			}
		}

	}

	if o.Token != nil {

		// form param token
		var frToken string
		if o.Token != nil {
			frToken = *o.Token
		}
		fToken := frToken
		if fToken != "" {
			if err := r.SetFormParam("token", fToken); err != nil {
				return err
			}
		}

	}

	// form param url
	frURL := o.URL
	fURL := frURL
	if fURL != "" {
		if err := r.SetFormParam("url", fURL); err != nil {
			return err
		}
	}

	if o.WikiPageEvents != nil {

		// form param wiki_page_events
		var frWikiPageEvents bool
		if o.WikiPageEvents != nil {
			frWikiPageEvents = *o.WikiPageEvents
		}
		fWikiPageEvents := swag.FormatBool(frWikiPageEvents)
		if fWikiPageEvents != "" {
			if err := r.SetFormParam("wiki_page_events", fWikiPageEvents); err != nil {
				return err
			}
		}

	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
