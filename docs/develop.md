## Development Guide

### 流程

![](flow.png)

### 新增工具

1. 提供工具对应的swagger文档于artifacts目录下，以版本号命名。

2. 在pkg/api/v1/types.go中新增对应的工具名称，如"github"，并检查interface下是否已经定义此工具需要的接口，新增接口需要在not_implement下同样定义一个。

3. 在pkg/generator下新增此工具的类型。

4. 执行`go run main.go gen -t toolName`来生成客户端。

5. 在对应生成的目录下实现相应的adapter，可以参照其他工具，最后需要将adapter保存到templates中。

6. 在pkg下新增add_toolName.go完成注册。

### 开发注意事项

1. 目前版本为 v1，但是目前不会特意维护一个版本分支，以 master 为主分支进行开发
2. 如果以后进入了 v2 版本，可以再 checkout 出 v1 或者 release-v1 分支进行归档
3. 进入 v2 版本后，v1 版本的 bug 修复将会在 v1 或者 release-v1 上进行
4. 目前版本管理通过 tag 来进行管理