# DevOps clients repository

The main purposes of this repository are:

- Maintain specific client code logic
- Unify interfaces making it transparent to the end user
- Simplify dependencies among them and use the same http client: https://github.com/go-resty/resty

## Code repo structure

This repository currently does not have a main file and therefore cannot be executed directly.
If the interfaces are all standard among clients we should be able to create a simple cli to use the given clients

#### pkg

`pkg` should store all the main code for the repository. Each client should have its own `package` and folder.  
A `generic` folder will keep useful common code among all clients and it should not import any client code, and can be imported by client code  
All useful interfaces will remain inside the `pkg` package For example:

```
├── README.md
└── pkg
    ├── ace
    ├── bitbucket
    ├── generic
    │   └── maps.go
    ├── github
    │   ├── client.go
    │   └── client_test.go
    └── interface.go
```

## Best practices

* All clients should integrate [resty](https://github.com/go-resty/resty) http client instead of standard library `http.Client`
* Using `resty` will naturally give access to inject `http.RoundTripper` as transport [as seen here](https://godoc.org/gopkg.in/resty.v1#Client.SetTransport)
* The clients should only be exposed as interfaces defined by `pkg/interface.go` and direct usage of implementations are not encouraged
* All packages should have a `doc.go` file with comments regarding the package contents
* All public functions and types should have correct comments. Public functions should also have an example of the return using comments for example:
```
func someFunc() int {
    return 1
    // Output:
    // 1
}
```

* All clients should strive for at least 80% test coverage
* Each client should register itself to the factory adding a `add_client.go` file invoke `pkg/AddClient` function 
* A good practice is to generate examples using `example_test.go` like seen here [godoc](https://golang.org/pkg/strings/#Map) [code](https://golang.org/src/strings/example_test.go)


## Unit tests

* Use [ginkgo](https://onsi.github.io/ginkgo/) and [gomega](https://onsi.github.io/gomega/) for unit tests structure and validation
* Use [gomock](https://github.com/golang/mock) for mocking generation and expectation. To use with gingko [check this article](https://onsi.github.io/ginkgo/#integrating-with-gomock)
* When using `gomock` generate mocks in `mock` folder

## Versioning

Using `semver`, the versioning of this repo works in this way:
* `master` branch only gives `build` versions, based on the latest version and adding a build serial number e.g `v0.1-b.1` where `b.1` is build number 1
* `release-*` branches will give definitive versions described by the name of the branch, e.g `release-1.2` would generate `v1.2.0` , `v1.2.1` etc.
* Pull requests or any other branches should not generate versions.

## Contributing

[contributing](CONTRIBUTING.md)