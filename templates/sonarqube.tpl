package {{.Version}}

import (
	"context"
	"errors"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/go-logr/logr"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/generic"
	"bitbucket.org/mathildetech/devops-client/pkg/sonarqube/{{.Version}}/client"
	"bitbucket.org/mathildetech/devops-client/pkg/sonarqube/{{.Version}}/client/operations"
	"bitbucket.org/mathildetech/devops-client/pkg/sonarqube/{{.Version}}/models"
	"bitbucket.org/mathildetech/devops-client/pkg/transport"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	v1.NotImplement
	logger     logr.Logger
	client     *client.Sonarqube
	opts       *v1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ v1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() v1.ClientFactory {
	return func(opts *v1.Options) v1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

func (c *Client) listProjectBranches(ctx context.Context, projectKey string) ([]*models.ProjectBranch, error) {
	params := operations.
		NewGetProjectBranchesListParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithProject(projectKey)
	branchesOK, err := c.client.Operations.GetProjectBranchesList(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return branchesOK.Payload.Branches, nil
}

func (c *Client) genericReport(ctx context.Context, projectKey string, branch models.ProjectBranch) (v1.CodeQualityCondition, error) {
	condition := &v1.CodeQualityCondition{}
	condition.Name = branch.Name
	condition.Type = v1.TypeCodeQualityReport
	condition.Branch = branch.Name
	condition.IsMain = branch.IsMain

	projectQualityGate, err := c.GetProjectQualityGateWithBranch(ctx, projectKey, branch.Name)
	if err != nil {
		return v1.CodeQualityCondition{}, err
	}

	basicData, err := c.GetProjectWithBranch(ctx, projectKey, branch.Name)
	if err != nil {
		return v1.CodeQualityCondition{}, err
	}

	metric, err := c.getMetricData(ctx, projectKey, branch.Name)
	if err != nil {
		return v1.CodeQualityCondition{}, err
	}

	analyzeTime, err := c.getLastAnalysisDate(basicData)
	if err != nil {
		return v1.CodeQualityCondition{}, err
	}

	condition.LastAttempt = analyzeTime
	// condition.Visibility = basicData.Visibility
	condition.QualityGate = projectQualityGate.Name
	condition.Status = metric["alert_status"]

	c.exactAnalysisDataToMetrics(condition, metric)
	return *condition, nil
}

func (c *Client) valueToLevel(key string) (level string) {
	levelMap := map[string]string{
		"1.0": "A",
		"2.0": "B",
		"3.0": "C",
		"4.0": "D",
		"5.0": "E",
	}
	if level, ok := levelMap[key]; ok {
		return level
	}
	return ""
}

func (c *Client) exactAnalysisDataToMetrics(condition *v1.CodeQualityCondition, metric map[string]string) {
	condition.Metrics = make(map[string]v1.CodeQualityAnalyzeMetric)
	condition.Metrics[v1.SonarQubeLanguages] = v1.CodeQualityAnalyzeMetric{
		Name:  v1.SonarQubeLanguages,
		Value: metric["ncloc_language_distribution"],
	}
	condition.Metrics[v1.SonarQubeNewBugs] = v1.CodeQualityAnalyzeMetric{
		Name:  v1.SonarQubeNewBugs,
		Value: metric["new_bugs"],
		Level: c.valueToLevel(metric["new_reliability_rating"]),
	}
	condition.Metrics[v1.SonarQubeNewVulnerabilities] = v1.CodeQualityAnalyzeMetric{
		Name:  v1.SonarQubeNewVulnerabilities,
		Value: metric["new_vulnerabilities"],
		Level: c.valueToLevel(metric["new_security_rating"]),
	}
	condition.Metrics[v1.SonarQubeNewCodeSmells] = v1.CodeQualityAnalyzeMetric{
		Name:  v1.SonarQubeNewCodeSmells,
		Value: metric["new_code_smells"],
		Level: c.valueToLevel(metric["new_maintainability_rating"]),
	}
	condition.Metrics[v1.SonarQubeNewCoverage] = v1.CodeQualityAnalyzeMetric{
		Name: v1.SonarQubeNewCoverage,

		Value: metric["new_coverage"],
	}
	condition.Metrics[v1.SonarQubeNewDuplication] = v1.CodeQualityAnalyzeMetric{
		Name:  v1.SonarQubeNewDuplication,
		Value: metric["new_duplicated_lines_density"],
	}
	// old metric
	condition.Metrics[v1.SonarQubeBugs] = v1.CodeQualityAnalyzeMetric{
		Name:  v1.SonarQubeBugs,
		Value: metric["bugs"],
		Level: c.valueToLevel(metric["reliability_rating"]),
	}
	condition.Metrics[v1.SonarQubeVulnerabilities] = v1.CodeQualityAnalyzeMetric{
		Name:  v1.SonarQubeVulnerabilities,
		Value: metric["vulnerabilities"],
		Level: c.valueToLevel(metric["security_rating"]),
	}
	condition.Metrics[v1.SonarQubeCodeSmells] = v1.CodeQualityAnalyzeMetric{
		Name:  v1.SonarQubeCodeSmells,
		Value: metric["code_smells"],
		Level: c.valueToLevel(metric["sqale_rating"]),
	}
	condition.Metrics[v1.SonarQubeCoverage] = v1.CodeQualityAnalyzeMetric{
		Name:  v1.SonarQubeCoverage,
		Value: metric["coverage"],
	}
	condition.Metrics[v1.SonarQubeDuplication] = v1.CodeQualityAnalyzeMetric{
		Name:  v1.SonarQubeDuplication,
		Value: metric["duplicated_lines_density"],
	}
}

func (c *Client) getLastAnalysisDate(project *models.Component) (date time.Time, err error) {
	if project.AnalysisDate != "" {
		date, err = time.Parse(v1.CodeQualityTime, project.AnalysisDate)
		if err != nil {
			return
		}
	}
	return
}

func (c *Client) getMetricData(ctx context.Context, projectKey, branch string) (map[string]string, error) {
	metricKeys := []string{
		"alert_status",
		"code_smells",
		"new_code_smells",
		"sqale_rating",
		"new_maintainability_rating",
		"bugs",
		"new_bugs",
		"reliability_rating",
		"new_reliability_rating",
		"vulnerabilities",
		"new_vulnerabilities",
		"security_rating",
		"new_security_rating",
		"coverage",
		"new_coverage",
		"duplicated_lines_density",
		"new_duplicated_lines_density",
		"ncloc_language_distribution",
	}

	params := operations.
		NewGetMeasuresComponentParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithComponent(&projectKey).
		WithMetricKeys(strings.Join(metricKeys, ","))
	projectMetricOK, err := c.client.Operations.GetMeasuresComponent(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := make(map[string]string)
	for _, m := range projectMetricOK.Payload.Component.Measures {
		key := m.Metric
		value := m.Value
		if value == "" {
			value = m.Periods[0].Value
		}
		result[key] = value
	}

	return result, nil
}

func (c *Client) GetProjectWithBranch(ctx context.Context, projectKey, branch string) (*models.Component, error) {
	params := operations.
		NewGetComponentsShowParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithComponent(&projectKey)
	projectOK, err := c.client.Operations.GetComponentsShow(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return projectOK.Payload.Component, nil
}

func (c *Client) GetProjectQualityGateWithBranch(ctx context.Context, projectKey, branch string) (*models.QualityGate, error) {
	params := operations.
		NewGetQualitygatesGetByProjectParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithProject(projectKey)
	projectQualityGateOK, err := c.client.Operations.GetQualitygatesGetByProject(params, c.authInfo)
	if err != nil {
		return nil, err
	}

	return projectQualityGateOK.Payload.QualityGate, nil
}

// GetProjectReport implements CodeQualityService
func (c *Client) GetProjectReport(ctx context.Context, projectKey string) ([]v1.CodeQualityCondition, error) {
	branches, err := c.listProjectBranches(ctx, projectKey)
	if err != nil {
		return nil, err
	}

	coditions := make([]v1.CodeQualityCondition, 0, len(branches))
	for _, branch := range branches {
		codition, err := c.genericReport(ctx, projectKey, *branch)
		if err != nil {
			now := time.Now()
			codition.Status = "Error"
			codition.Message = err.Error()
			codition.LastAttempt = now
		}
		coditions = append(coditions, codition)
	}
	return coditions, nil
}

// CheckProjectExist implements CodeQualityService
func (c *Client) CheckProjectExist(ctx context.Context, projectKey string) (bool, error) {
	project, err := c.GetProjectWithBranch(ctx, projectKey, "")
	if err != nil {
		return false, err
	}

	if project == nil {
		return false, nil
	}

	return true, nil
}

// GetLastAnalysisDate implements CodeQualityService
func (c *Client) GetLastAnalysisDate(ctx context.Context, projectKey string) (*time.Time, error) {
	project, err := c.GetProjectWithBranch(ctx, projectKey, "")
	if err != nil {
		return nil, err
	}

	currentDate, err := c.getLastAnalysisDate(project)
	if err != nil {
		return nil, err
	}

	branches, err := c.listProjectBranches(ctx, projectKey)
	if err != nil {
		return &currentDate, nil
	}

	for _, branch := range branches {
		if branch.AnalysisDate != "" {
			date, err := time.Parse(v1.CodeQualityTime, branch.AnalysisDate)
			if err != nil {
				continue
			}

			if date.After(currentDate) {
				currentDate = date
			}
		}
	}

	return &currentDate, nil
}

func (c *Client) listAllProjects(ctx context.Context) ([]*models.Component, error) {
	var (
		page     = int32(0)
		pageSize = int32(100)
		projects = []*models.Component{}
	)

	for {
		page = page + 1
		parmas := operations.
			NewGetProjectsSearchParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithP(&page).
			WithPs(&pageSize)
		projectsOK, err := c.client.Operations.GetProjectsSearch(parmas, c.authInfo)
		if err != nil {
			return nil, err
		}

		projects = append(projects, projectsOK.Payload.Components...)

		if projectsOK.Payload.Paging.Total < pageSize {
			break
		}
	}

	return projects, nil
}

var domainWithPathsRegex = regexp.MustCompile("(.*://)?(.*)")

func (c *Client) getCorrespondProjectKey(repository *v1.CodeRepository) string {
	repoUrl := repository.Spec.Repository.HTMLURL
	r := domainWithPathsRegex.FindStringSubmatch(repoUrl)

	domainWithPaths := r[2]
	return strings.Replace(domainWithPaths, "/", "-", -1)
}

func (c *Client) generateCodeQualityProject(project *models.Component, repository *v1.CodeRepository, binding *v1.CodeQualityBinding) v1.CodeQualityProject {
	codeQualityProject := v1.CodeQualityProject{
		Name:      binding.Name + "-" + project.Key,
		Namespace: binding.Namespace,
		Spec: v1.CodeQualityProjectSpec{
			CodeQualityBinding: v1.LocalObjectReference{Name: binding.Name},
			CodeQualityTool:    v1.LocalObjectReference{Name: binding.Spec.CodeQualityTool.Name},
			CodeRepository:     v1.LocalObjectReference{Name: repository.Name},
			Project: v1.CodeQualityProjectInfo{
				CodeAddress: repository.Spec.Repository.FullName,
				ProjectKey:  project.Key,
				ProjectName: project.Name,
			},
		},
	}

	lastAnalysisDate, err := c.getLastAnalysisDate(project)
	if err == nil {
		codeQualityProject.Spec.Project.LastAnalysis = lastAnalysisDate
	}

	return codeQualityProject
}

// GetCorrespondCodeQualityProjects implements CodeQualityService
func (c *Client) GetCorrespondCodeQualityProjects(ctx context.Context, repositoryList *v1.CodeRepositoryList, binding *v1.CodeQualityBinding) ([]v1.CodeQualityProject, error) {
	projects, err := c.listAllProjects(ctx)
	if err != nil {
		return nil, err
	}

	projectsNeedToBeSynced := make([]v1.CodeQualityProject, 0)
	for _, repo := range repositoryList.Items {
		projectKey := c.getCorrespondProjectKey(&repo)
		for _, project := range projects {
			if projectKey == project.Key {
				codeQualityProject := c.generateCodeQualityProject(project, &repo, binding)
				lastAnalysisDate, err := c.GetLastAnalysisDate(ctx, project.Key)
				if err == nil {
					date := *lastAnalysisDate
					codeQualityProject.Spec.Project.LastAnalysis = date
				}

				projectsNeedToBeSynced = append(projectsNeedToBeSynced, codeQualityProject)
			}
		}
	}
	return projectsNeedToBeSynced, nil
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		return generic.CheckService(c.opts.BasicConfig.Host, nil)
	}
	return nil, errors.New("host config error")
}
