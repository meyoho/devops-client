package {{.Version}}

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-logr/logr"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/generic"
	"bitbucket.org/mathildetech/devops-client/pkg/taiga/{{.Version}}/client"
	"bitbucket.org/mathildetech/devops-client/pkg/taiga/{{.Version}}/client/operations"
	"bitbucket.org/mathildetech/devops-client/pkg/taiga/{{.Version}}/models"
	"bitbucket.org/mathildetech/devops-client/pkg/transport"
	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	v1.NotImplement
	logger     logr.Logger
	client     *client.Taiga
	opts       *v1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
	userToken  *models.UserToken
}

var _ v1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() v1.ClientFactory {
	return func(opts *v1.Options) v1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			taigaClient := &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}

			if opts.BasicAuth != nil {
				userToken, _ := taigaClient.Auth(opts.BasicAuth.Username, opts.BasicAuth.Password)
				if userToken != nil {
					taigaClient.userToken = userToken
					taigaClient.authInfo = openapi.BearerToken(userToken.AuthToken)
				}
			}
			return taigaClient
		}

		return &Client{client: client.Default}
	}
}

// Auth for token
func (c *Client) Auth(username, password string) (*models.UserToken, error) {
	user := &models.User{
		Username: username,
		Password: password,
		Type:     "normal",
	}
	authParma := operations.
		NewPostAuthParams().
		WithHTTPClient(c.httpClient).
		WithUser(user)
	authOK, err := c.client.Operations.PostAuth(authParma, c.authInfo)
	if err != nil {
		return nil, err
	}

	return authOK.Payload, nil
}

// GetProjects implements ProjectService
func (c *Client) GetProjects(ctx context.Context, page, pagesize string) (*v1.ProjectDataList, error) {
	var (
		projectPage     int64
		projectPageSize int64
		err             error
	)

	if page != "" && pagesize != "" {
		projectPage, err = strconv.ParseInt(page, 10, 64)
		projectPageSize, err = strconv.ParseInt(pagesize, 10, 64)
		if err != nil {
			return nil, err
		}
	} else {
		projectPage = 1
		projectPageSize = 10
	}

	start := (projectPage - 1) * projectPageSize
	end := projectPage * projectPageSize

	projectsParam := operations.
		NewGetProjectsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient)
	if c.userToken != nil {
		projectsParam.SetMember(&c.userToken.ID)
	}
	projectsOK, err := c.client.Operations.GetProjects(projectsParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := &v1.ProjectDataList{}

	var count int64
	for _, project := range projectsOK.Payload {
		if start <= count && count < end {
			projectData := v1.ProjectData{}
			projectData.Name = project.Name
			projectData.Annotations = map[string]string{
				"self":        fmt.Sprintf("/projects/%s/timeline", project.Slug),
				"id":          strconv.Itoa(int(project.ID)),
				"description": project.Description,
			}
			result.Items = append(result.Items, projectData)
		}
	}

	return result, nil
}

// CreateProject implements ProjectService
func (c *Client) CreateProject(ctx context.Context, projectName, projectDescription, projectLead, projectKey string) (*v1.ProjectData, error) {
	project := &models.ProjectCreate{
		Name:        projectName,
		Description: projectDescription,
	}
	projectParam := operations.
		NewPostProjectsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithProject(project)
	projectOK, err := c.client.Operations.PostProjects(projectParam, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := &v1.ProjectData{}
	result.Name = projectName
	result.Annotations = map[string]string{
		"self":        fmt.Sprintf("/prjects/%s/timeline", projectOK.Payload.Slug),
		"id":          fmt.Sprintf("%d", projectOK.Payload.ID),
		"description": projectDescription,
	}

	return result, nil
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		return generic.CheckService(c.opts.BasicConfig.Host, nil)
	}
	return nil, errors.New("host config error")
}
