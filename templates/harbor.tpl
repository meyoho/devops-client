// Code generated
package {{.Version}}

import (
	"context"
	"encoding/base64"
    "encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/go-logr/logr"

	v1 "bitbucket.org/mathildetech/devops-client/pkg/api/v1"
	"bitbucket.org/mathildetech/devops-client/pkg/generic"
	"bitbucket.org/mathildetech/devops-client/pkg/harbor/{{.Version}}/client"
	"bitbucket.org/mathildetech/devops-client/pkg/harbor/{{.Version}}/client/products"
	"bitbucket.org/mathildetech/devops-client/pkg/harbor/{{.Version}}/models"
	"bitbucket.org/mathildetech/devops-client/pkg/transport"

	"github.com/go-openapi/runtime"
	openapi "github.com/go-openapi/runtime/client"
)

// Client is devops tool client
type Client struct {
	v1.NotImplement
	logger     logr.Logger
	client     *client.Harbor
	opts       *v1.Options
	authInfo   runtime.ClientAuthInfoWriter
	httpClient *http.Client
}

var _ v1.Interface = &Client{}

// NewClient new devops tool client
func NewClient() v1.ClientFactory {
	return func(opts *v1.Options) v1.Interface {
		if opts != nil {
			config := client.DefaultTransportConfig()
			if opts.BasicConfig != nil {
				if opts.BasicConfig.Host != "" {
					config.WithHost(opts.BasicConfig.Host)
				}
				if opts.BasicConfig.BasePath != "" {
					config.WithBasePath(opts.BasicConfig.BasePath)
				}
				if len(opts.BasicConfig.Schemes) != 0 {
					config.WithSchemes(opts.BasicConfig.Schemes)
				}
			}

			var auth runtime.ClientAuthInfoWriter
			if opts.BasicAuth != nil {
				auth = openapi.BasicAuth(opts.BasicAuth.Username, opts.BasicAuth.Password)
			}

			if opts.BearerToken != nil {
				auth = openapi.BearerToken(opts.BearerToken.Token)
			}

			if opts.APIKey != nil {
				auth = openapi.APIKeyAuth(opts.APIKey.Name, opts.APIKey.In, opts.APIKey.Value)
			}

			transport := transport.New(config.Host, config.BasePath, config.Schemes)
			transport.SetDebug(true)
			transport.SetLogger(opts.Logger)
			client := client.New(transport, nil)

			return &Client{
				logger:     opts.Logger,
				client:     client,
				opts:       opts,
				authInfo:   auth,
				httpClient: opts.Client,
			}
		}

		return &Client{client: client.Default}
	}
}

func (c *Client) listAllProjects(ctx context.Context) ([]*models.Project, error) {
	var (
		page     = int32(0)
		pageLen  = int32(100)
		projects = []*models.Project{}
	)

	for {
		page = page + 1
		projectsParam := products.
			NewGetProjectsParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithPage(&page).
			WithPageSize(&pageLen)
		projectsOK, err := c.client.Products.GetProjects(projectsParam, c.authInfo)
		if err != nil {
			return nil, err
		}

		projects = append(projects, projectsOK.Payload...)

		if len(projectsOK.Payload) < int(pageLen) {
			break
		}
	}

	return projects, nil
}

func (c *Client) listAllRepos(ctx context.Context, projectID int32) ([]*models.Repository, error) {
	var (
		page    = int32(0)
		pageLen = int32(100)
		repos   = []*models.Repository{}
	)

	for {
		page = page + 1
		reposParams := products.
			NewGetRepositoriesParams().
			WithContext(ctx).
			WithHTTPClient(c.httpClient).
			WithProjectID(projectID).
			WithPage(&page).
			WithPageSize(&pageLen)
		reposOK, err := c.client.Products.GetRepositories(reposParams, c.authInfo)
		if err != nil {
			return nil, err
		}

		repos = append(repos, reposOK.Payload...)

		if len(reposOK.Payload) < int(pageLen) {
			break
		}
	}

	return repos, nil
}

// GetImageRepos implments ImageRegistryService interface
func (c *Client) GetImageRepos(ctx context.Context) (*v1.ImageRegistryBindingRepositories, error) {
	projects, err := c.listAllProjects(ctx)
	if err != nil {
		return nil, err
	}

	result := &v1.ImageRegistryBindingRepositories{}
	for _, project := range projects {
		repos, err := c.listAllRepos(ctx, project.ProjectID)
		if err != nil {
			return nil, err
		}

		for _, repo := range repos {
			result.Items = append(result.Items, repo.Name)
		}
	}

	return result, nil
}

// GetImageTags implments ImageRegistryService interface
func (c *Client) GetImageTags(ctx context.Context, repository string) ([]v1.ImageTag, error) {
	tagParmas := products.
		NewGetRepositoriesRepoNameTagsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithRepoName(repository)
	tags, err := c.client.Products.GetRepositoriesRepoNameTags(tagParmas, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := make([]v1.ImageTag, 0, len(tags.Payload))
	for _, tag := range tags.Payload {
		imageTag := v1.ImageTag{
			Name:   tag.Name,
			Digest: strings.TrimPrefix(tag.Digest, generic.ImageDigestSignature),
			Author: tag.Author,
			Size:   generic.ConvertSizeToString(tag.Size),
		}

		creatdAt, _ := time.Parse(time.RFC3339Nano, tag.Created)
		imageTag.CreatedAt = creatdAt
		imageTag.ScanStatus = v1.ImageTagScanStatusNotScan

		if tag.ScanOverview != nil {
			if tag.ScanOverview.ScanStatus == string(v1.ImageTagScanStatusError) {
				imageTag.ScanStatus = v1.ImageTagScanStatusError
				imageTag.Message = c.GetScanJobLog(tag.ScanOverview.JobID)
			} else {
				imageTag.ScanStatus = v1.ImageTagScanStatus(tag.ScanOverview.ScanStatus)
				imageTag.Level = int(tag.ScanOverview.Severity)
				if tag.ScanOverview.Components != nil {
					for _, s := range tag.ScanOverview.Components.Summary {
						summary := v1.Summary{Severity: int(s.Severity), Count: int(s.Count)}
						imageTag.Summary = append(imageTag.Summary, summary)
					}
				}
			}
		}
		result = append(result, imageTag)
	}

	return result, nil
}

// GetScanJobLog TODO
func (c *Client) GetScanJobLog(jobID int64) string {
	// TDOD
	return ""
}

// TriggerScanImage implments ImageRegistryService interface
func (c *Client) TriggerScanImage(ctx context.Context, repositoryName, tag string) *v1.ImageResult {
	scanParams := products.
		NewPostRepositoriesRepoNameTagsTagScanParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithRepoName(repositoryName).
		WithTag(tag)
	scanOK, err := c.client.Products.PostRepositoriesRepoNameTagsTagScan(scanParams, c.authInfo)

	result := &v1.ImageResult{}
	if err != nil {
		result.StatusCode = http.StatusInternalServerError
		result.Message = err.Error()
		return result
	}

	result.StatusCode = http.StatusOK
	result.Message = scanOK.Error()
	return result
}

// GetVulnerability implments ImageRegistryService interface
func (c *Client) GetVulnerability(ctx context.Context, repositoryName, tag string) (*v1.VulnerabilityList, error) {
	vulParams := products.
		NewGetRepositoriesRepoNameTagsTagVulnerabilityDetailsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithRepoName(repositoryName).
		WithTag(tag)
	vulDetail, err := c.client.Products.GetRepositoriesRepoNameTagsTagVulnerabilityDetails(vulParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := &v1.VulnerabilityList{}
	for _, item := range vulDetail.Payload {
		vulner := v1.Vulnerability{
			ID:          item.ID,
			Severity:    int(item.Severity),
			Package:     item.Package,
			Version:     item.Version,
			Description: item.Description,
		}

		result.Items = append(result.Items, vulner)
	}

	return result, nil
}

// GetImageProjects implments ImageRegistryService interface
func (c *Client) GetImageProjects(ctx context.Context) (*v1.ProjectDataList, error) {
	projects, err := c.listAllProjects(ctx)
	if err != nil {
		return nil, err
	}

	result := &v1.ProjectDataList{}
	for _, project := range projects {
		data, err := generic.MarshalToMapString(project)
		if err != nil {
			return nil, err
		}

		result.Items = append(result.Items, v1.ProjectData{Data: data, Name: project.Name, ProjectID: int(project.ProjectID)})
	}

	return result, nil
}

// CreateImageProject implments ImageRegistryService interface
func (c *Client) CreateImageProject(ctx context.Context, name string) (*v1.ProjectData, error) {
	createParams := products.
		NewPostProjectsParams().
		WithContext(ctx).
		WithHTTPClient(c.httpClient).
		WithProject(&models.ProjectReq{ProjectName: name})
	_, err := c.client.Products.PostProjects(createParams, c.authInfo)
	if err != nil {
		return nil, err
	}

	result := &v1.ProjectData{}
	return result, nil
}

func (c *Client) Available(_ context.Context) (*v1.HostPortStatus, error) {
	if c.opts.BasicConfig != nil {
		url := c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host
		return generic.CheckService(url, nil)
	}
	return nil, errors.New("host config error")
}

func (c *Client) Authenticate(ctx context.Context) (*v1.HostPortStatus, error) {
	var (
		url    string
		header map[string]string
	)

	if c.opts.BasicConfig != nil {
		url = c.opts.BasicConfig.Schemes[0] + "://" + c.opts.BasicConfig.Host + "/api/statistics"
	}

	if c.opts.BasicAuth != nil {
		username, password := c.opts.BasicAuth.Username, c.opts.BasicAuth.Password
		encoded := base64.StdEncoding.EncodeToString([]byte(username + ":" + password))
		header = map[string]string{
			"Authorization": fmt.Sprintf("Basic %s", encoded),
		}
	}

	return generic.CheckService(url, header)
}
