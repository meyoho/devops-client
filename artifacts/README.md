## 注意

官方给出的 [swagger.yaml](https://raw.githubusercontent.com/goharbor/harbor/v1.8.4/docs/swagger.yaml) 
中缺少一些字段，例如：`oidc_scope` 等。