// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

// global variables for pipeline
// image can be used for promoting...
def IMAGE
def RELEASE_VERSION
def RELEASE_BUILD
def release
pipeline {
	agent { label 'golang-1.12' }

	options {
		buildDiscarder(logRotator(numToKeepStr: '10'))
		disableConcurrentBuilds()
		skipDefaultCheckout()
	}

	parameters {
		booleanParam(name: 'DEBUG', defaultValue: false, description: 'Debug will not do final changes...')
	}
	//(optional) 环境变量
	environment {
		FOLDER = '.'

		// for building an scanning
		REPOSITORY = "devops-client"
		OWNER = "mathildetech"

		BITBUCKET_FEEDBACK_ACCOUNT = "alaudabot"
		DINGDING_BOT = "devops-chat-bot"
		TAG_CREDENTIALS = "alaudabot-bitbucket"

		// devops cli image
		IMAGE_REPOSITORY = "alaudak8s/devops-cli"

		// go lang 1.12 proxy and modules support
		GO111MODULE = "on"
		GOPROXY = "https://athens.acp.alauda.cn"

		// charts pipeline name
		VERSION = "v1.0"
	}
	// stages
	stages {
		stage('Checkout') {
			steps {
				script {
					dir(FOLDER) {
						container('tools') {
							def scmVars
							retry(2) { scmVars = checkout scm }
							release = deploy.release(scmVars)

							release.config_data.setKey("acp-version", VERSION)
							release.calculate()
							// WARNING: Temporary hack for now. MUST REMOVE THIS LATER
							if (release.is_master) {
								def genVersion = sh returnStdout: true, script: "gitversion patch ${VERSION}"
								genVersion = genVersion.replace("\n", "")
								release.version = genVersion
							}

							RELEASE_BUILD = release.version
							RELEASE_VERSION = release.majorVersion
							// echo "release ${RELEASE_VERSION} - release build ${RELEASE_BUILD}"
							echo """
								release ${RELEASE_VERSION}
								version ${release.version}
								is_release ${release.is_release}
								is_build ${release.is_build}
								is_master ${release.is_master}
								deploy_env ${release.environment}
								auto_test ${release.auto_test}
								environment ${release.environment}
								majorVersion ${release.majorVersion}
							"""
							// copying kubectl from tools
							sh "cp /usr/local/bin/kubectl ."
						}
					}
				}
			}
		}
		stage('CI'){
			steps {
				script {
					dir(FOLDER) {
						container('golang') {
							sh "make fmt"
							sh "make vet"
							sh "make test"
						}
					}
				}
			}
		}
		stage('Build CLI'){
			steps {
				script {
					dir(FOLDER) {
					    container('golang') {
                            sh script: "make build-cli-linux", label: "Build DevOps CLI"
						}

						container('tools') {
                            DEVOPS_CLI = deploy.dockerBuildWithRegister(
                                address: IMAGE_REPOSITORY,
                                tag: "${RELEASE_BUILD}"
                                ).setArg("commit_id", "${release.commit}").setArg("version", "${RELEASE_BUILD}")
                            DEVOPS_CLI.start().push()
						}
					}
				}
			}
		}
		stage('Tag git') {
			when {
				expression { release.shouldTag() }
			}
			steps {
				script {
					dir(FOLDER) {
						container('tools') {
							deploy.gitTag(
								TAG_CREDENTIALS,
								RELEASE_BUILD,
								OWNER,
								REPOSITORY
								)
						}
					}
				}
			}
		}
	}

	post {
		success {
			dir(FOLDER) {
				script {
					container('tools') {
						def msg = "流水线完成了"
						deploy.notificationSuccess(REPOSITORY, DINGDING_BOT, msg, RELEASE_BUILD)
						if (release != null) { release.cleanEnv() }
					}
				}
			}
		}

		failure {
			dir(FOLDER) {
				script {
					container('tools') {
						deploy.notificationFailed(REPOSITORY, DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
						if (release != null) { release.cleanEnv() }
					}
				}
			}
		}
	}
}
